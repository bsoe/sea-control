Series Elastic Actuator Control
===============================

This c++ library is for force control of series elastic actuators, using lead compensator and disturbance observer (DOB). 

Controller
----------

This library enables high performance force control of series elastic actuators. It uses a lead compensator for closed loop torque control, assuming spring deflection to measure output torque. It uses a disturbance observer to handle the moving output load. Please see the work of Nicholas Paine and Luis Sentis for details on the controller scheme.

![Block Diagram](block_diagram.png)
![Force Response for Fixed Load](fixed_response.png)
![Force Response for Free Load](free_response.png)

Build
-----

Nothing to build. Everything is a header for now, because the code is pretty lightweight. A CMakeLists.txt is included if you want to move the implementation into cpp files and build a library instead.

The tests and the applications are built with cmake, in the usual fashion.

        cd test/some-test
        mkdir build && cd build
        cmake .. && make
        ./some-test

Tests
-----

The tests contains simulation of the controller, and contain Matlab scripts to plot the bode from the simulation output. The following simulations are included for both a fixed output load, and a free moving output load:

* Simulation of open loop torque control
* Simulation of lead compensator
* Simulation of lead compensator + disturbance observer

Applications
------------

These are [example applications], that show how to use this library to:

1. Collect data for plant identification
2. Design a controller for the plant

The applications contain some scripts that plot and export figures. Before running the script, initialize the plotting git submodule.

        git submodule update --init --recursive

[example applications]:http://bitbucket.org/bsoe/sea-control/src/master/applications/README.md

