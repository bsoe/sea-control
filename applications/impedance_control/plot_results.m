clc; clear; close all;

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

Ts = data(2,1)-data(1,1);
istart = 1;
iend   = size(data,1);

time             = data(istart:iend,1);
desired_position = data(istart:iend,2);
output_position  = data(istart:iend,3);
motor_position   = data(istart:iend,4);
%spring_count  = data(istart:iend,3);
desired_torque   = data(istart:iend,5);
spring_torque    = data(istart:iend,6);
controller_torque = data(istart:iend,7);
%friction_torque  = data(istart:iend,8);
total_torque     = data(istart:iend,8);
actual_torque    = data(istart:iend,9);
%{
ATI_force_x      = data(istart:iend,10);
ATI_force_y      = data(istart:iend,11);
ATI_force_z      = data(istart:iend,12);
ATI_torque_x     = data(istart:iend,13);
ATI_torque_y     = data(istart:iend,14);
ATI_torque_z     = data(istart:iend,15);
%}

N = length(time);

%% plot
figure(1)
plot(time, desired_position,...
     time, output_position);
 hold on;
legend('desired','measured')
xlabel('time (s)')
ylabel('position (rad)')


figure(2)
plot(time, desired_torque,...
     time, spring_torque);
 hold on;
legend('desired','measured')
xlabel('time (s)')
ylabel('torque (rad)')

%% hand fit sinusoid
wn_est = 2*pi*0.77;  %0.64; %0.77
phase_est = 0.48*pi;  %1.1 %0.2

figure(1)
plot(time, 0.9*max(output_position)*sin(wn_est*time-phase_est))
legend('desired','measured','est')

%% estimate the mass
k = 2;
mo = k/wn_est^2 %0.12 %0.85
bo = 0
