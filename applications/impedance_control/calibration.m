clc; clear; close all;

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

Ts = data(2,1)-data(1,1);
istart = find(data(:,2)>0.1,1)-1; % start at freq = 0.1 rad/s;
iend   = size(data,1); %round(380/Ts);

time             = data(istart:iend,0);
%frequency        = data(istart:iend,2);
desired_position = data(istart:iend,1);
output_position  = data(istart:iend,2);
motor_position   = data(istart:iend,3);
%spring_count  = data(istart:iend,3);
desired_torque   = data(istart:iend,4);
spring_torque    = data(istart:iend,5);
controller_torque = data(istart:iend,6);
%friction_torque  = data(istart:iend,8);
total_torque     = data(istart:iend,7);
actual_torque    = data(istart:iend,8);
ATI_force_x      = data(istart:iend,9);
ATI_force_y      = data(istart:iend,10);
ATI_force_z      = data(istart:iend,11);
ATI_torque_x     = data(istart:iend,12);
ATI_torque_y     = data(istart:iend,13);
ATI_torque_z     = data(istart:iend,14);

N = length(time);

%% plot
scaling = 1.15;
sensor_offset = 0.0791; %meters

figure
plot(time,spring_torque*scaling+0.1); hold on;
plot(time,...
    (ATI_force_x *sind(30)+ATI_force_y *cosd(30))*sensor_offset + ...
    +ATI_torque_x*cosd(30)-ATI_torque_y*sind(30))
legend('spring','force sensor')

return

figure
plot(time,(ATI_force_x*cosd(120)+ATI_force_y*sind(120))*sensor_offset); hold on;

figure
plot(time, ATI_torque_y*cosd(120)+ATI_torque_x*sin(120) )