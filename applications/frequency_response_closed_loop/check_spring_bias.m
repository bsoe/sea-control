clc; clear; close all;

data_file = 'build/spring_bias.txt';
data = importdata(data_file);

time   = data(:,1);
jitter = data(:,2);
spring = data(:,3);

figure
subplot(2,1,1)
plot(time, jitter)
ylabel('jitter')
subplot(2,1,2)
plot(time, spring)
ylabel('spring reading')
xlabel('time')

figure
[mu,sigma] = MyNormalFit(spring,true);
xlabel('motor position')
ylabel('# occurances')
title('histogram to find motor position offset')

mu
bias = median(spring)

line([bias bias],ylim,'Color','g')