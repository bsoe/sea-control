#include <sea/filter/LowPassFilter.h>
#include <sea/friction/CoulombFrictionCompensator.h>
#include <sea/Chirp.h>
#include "FindSpringBias.h"

#include <sea/DOBLeadController.h>

#include <simplecat/Master.h>
#include <simplecat/Elmo/Elmo.h>

#include <iostream>
#include <fstream>
#include <cmath>

/****************************************************************************/

sea::DOBLeadController controller;
sea::LowPassFilter motor_filter;
sea::CoulombFrictionCompensator friction;
sea::Chirp chirp;
std::ofstream data;

simplecat::Master master;
simplecat::Elmo_GoldWhistle elmo_gw;

std::ofstream data_file;

// torque constant, spring position / torque
const double torque_constant = 4.4;

// controller
const double wo = 17.5;
const double zo = 0.18;
const double wc = 87.5;
const double zc = 0.7071;
const double wq = 50.0;
const double zq = 0.7071;
const double Ts = 0.001;
const double min_output = -1000;
const double max_output =  1000;

// chirp setup
const double chirp_amplitude = 250;
const double chirp_f_min     = 0.005;
const double chirp_f_max     = 50;
const double chirp_duration  = 600;

// friction compensation
const double coulomb_deadzone =   5;
const double coulomb_positive =  75;
const double coulomb_negative = -75;

// jitter for finding spring zero position
std::ofstream spring_bias_file;
sea::FindSpringBias find_spring_bias;
const double jitter_start_amplitude = 300;
const double jitter_end_amplitude   = 50;
const double jitter_frequency       = 10;
const double jitter_duration        = 20;

// parameters
bool initialized = false;
int motor_position_offset  = 0;
int spring_position_offset = 0;
double desired_torque = 0;

/****************************************************************************/

void ctrl_c_handler(int s)
{
    std::cout << "exiting" << std::endl;
    master.stop();
}

/****************************************************************************/

unsigned int loop_counter = 0;
void control_callback()
{
    if (!elmo_gw.initialized()){ return; }

    if (!initialized){

        if (find_spring_bias.time() > find_spring_bias.duration()){

            double median = find_spring_bias.calculateBias();
            motor_position_offset  = elmo_gw.position_;
            spring_position_offset = median;
            initialized = true;

            std::cout << "=====initialization=====\n";
            std::cout << "motor position offset:  " << motor_position_offset << '\n';
            std::cout << "spring_position offset: " << spring_position_offset << '\n';
            std::cout << "=====initialization=====\n";
        } 
        else {
            double jitter_value = find_spring_bias.step(elmo_gw.absolute_position_);
            elmo_gw.target_torque_ = std::round(jitter_value);

            spring_bias_file << find_spring_bias.time() << '\t'
                             << jitter_value << '\t'
                             << elmo_gw.absolute_position_ << '\n';

            if(find_spring_bias.count()%500==0){
                std::cout << find_spring_bias.time() << '\t'
                          << jitter_value << '\t'
                          << elmo_gw.absolute_position_ << '\n';

            }
        }
        return;
    }

    // position and velocity
    int motor_position = elmo_gw.position_ - motor_position_offset;
    int spring_position = elmo_gw.absolute_position_ - spring_position_offset;
    double spring_torque = ((double)spring_position)/torque_constant;

    // friction compensation
    motor_filter.update(motor_position);
    double motor_filtered_position = motor_filter.out(0);
    double motor_filtered_velocity = motor_filter.out(0) - motor_filter.out(1);
    double friction_torque = friction.friction(motor_filtered_velocity);

    // chirp
    double desired_torque = chirp.step();
    double frequency = chirp.frequency();

    // controller
    double controller_torque = controller.update(desired_torque,spring_torque);
    double error_torque = desired_torque - spring_torque;

    // total torque
    elmo_gw.target_torque_ = 0;
    elmo_gw.target_torque_ += std::round(friction_torque);
    elmo_gw.target_torque_ += std::round(controller_torque);

    // write data to file
    double time = Ts*master.elapsedCycles();
    data_file << time << '\t'
              << frequency << '\t'
              << motor_position << '\t'
              << spring_position << '\t'
              << desired_torque << '\t'
              << spring_torque << '\t'
              << controller_torque << '\t'
              << friction_torque << '\t'
              << elmo_gw.target_torque_ << '\t'
              << elmo_gw.torque_ << '\n';

    if (loop_counter %500==0){

        std::cout << "===========================\n";
        std::cout << std::dec;
        std::cout << "time            : " << time << '\n';
        std::cout << "frequency       : " << frequency << '\n';
        std::cout << "motor    pos    : " << motor_position << '\n';
        std::cout << "spring   pos    : " << spring_position << '\n';
        std::cout << "motor    vel    : " << motor_filtered_velocity << '\n';      
        std::cout << "desired  torque : " << desired_torque << '\n';
        std::cout << "spring   torque : " << spring_torque << '\n';
        std::cout << "control  torque : " << controller_torque << '\n';
        std::cout << "friction torque : " << friction_torque << '\n';  
        std::cout << "total    torque : " << elmo_gw.target_torque_ << '\n';
        std::cout << "actual   torque : " << elmo_gw.torque_ << '\n';
    }

    ++loop_counter;

    if (chirp.time()>=chirp.duration()){
        master.stop();
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{
    // jitter
    spring_bias_file.open("spring_bias.txt");
    find_spring_bias.setAmplitude(jitter_start_amplitude, jitter_end_amplitude);
    find_spring_bias.setFrequency(jitter_frequency);
    find_spring_bias.setDuration( jitter_duration);
    find_spring_bias.setSamplingTime(Ts);

    // chirp setup
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(chirp_amplitude);
    chirp.setFrequencyRange(chirp_f_min, chirp_f_max);
    chirp.setDuration(chirp_duration);

    // controller
    controller.setSampleTime(Ts);
    controller.setOpenLoopPlant(wo,zo);
    controller.setClosedLoopPlant(wc,zc);
    controller.setDOB(wq,zq);
    controller.enableLead(true);
    controller.enableDOB(true);
    controller.enable(true);
    controller.setLimits(min_output,max_output);

    // velocity filter
    motor_filter.setButterworth(314,Ts);

    // friction
    friction.setBuffer(coulomb_deadzone);
    friction.setCoulombPositive(coulomb_positive);
    friction.setCoulombNegative(coulomb_negative);

    // data file
    data_file.open("data.txt");

    // ethercat master
    master.setCtrlCHandler(ctrl_c_handler);
    master.addSlave(0,0,&elmo_gw);
    elmo_gw.max_torque_ = 1000;

    //master.setThreadHighPriority();
    master.setThreadRealTime();
    master.activate();

    // run main loop
    master.run(control_callback, 1.0/Ts);

    // close data file
    data.close();

    std::cout << "run time : " << master.elapsedTime() << std::endl;
    std::cout << "updates  : " << master.elapsedCycles() << std::endl;
    std::cout << "frequency: " << master.elapsedCycles()/master.elapsedTime() << std::endl;

    return 0;
}

/****************************************************************************/
