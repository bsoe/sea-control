/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_FINDSPRINGBIAS_H_
#define SEA_FINDSPRINGBIAS_H_

#include <cmath>
#include <vector>
#include <algorithm>

namespace sea {

/** \brief FindSpringBias base class
 * sine wave jitter with decreasing amplitude
 * find the average 
 */
class FindSpringBias
{
public:

    FindSpringBias(){}
    virtual ~FindSpringBias(){}

    /** \brief Step the chirp by Ts.
     * \return The new value
     */
    virtual double step(double reading)
    {
        t_ = Ts_*((double)i_);
        if (t_>t_end_){return 0;}

        double A = A_start_ + (A_end_ - A_start_)*(t_/t_end_);
        double value_ = A*sin(2*M_PI*freq_*t_);
        readings_[i_] = reading;
        ++i_;
        return value_;
    }

    /** \brief Reset the time to some point in the chirp */
    virtual void resetTime(double time = 0){
        i_ = std::round(time/Ts_);
        t_ = Ts_*((double)i_);
    }

    /** \brief Calculate the spring bias */
    virtual int calculateBias()
    {
        int median;
        size_t size = readings_.size();

        std::sort(readings_.begin(), readings_.end());

        if (size  % 2 == 0)
        {
          median = (readings_[size / 2 - 1] + readings_[size / 2]) / 2;
        }
        else 
        {
          median = readings_[size / 2];
        }
        return median;
    }

    /** \brief Update the FindSpringBias
     * \param time Time */
    virtual const double& value(){
        return value_;
    }

    virtual const double& time(){
        return t_;
    }

    virtual const double& duration(){
        return t_end_;
    }
    
    virtual const unsigned long& count(){
        return i_;
    }

    virtual void setSamplingTime(double Ts){
        Ts_ = Ts;
        resetTime(t_);
        readings_.resize(std::round(t_end_/Ts_));
    }

    virtual void setAmplitude(double start_amplitude, double end_amplitude){
        A_start_ = start_amplitude;
        A_end_   = end_amplitude;
    }

    virtual void setFrequency(double freq){
        freq_ = freq;
    }

    virtual void setDuration(double end_time){
        t_end_  = end_time;
        readings_.resize(std::round(t_end_/Ts_));
    }

protected:

    unsigned long i_ = 0;
    double t_        = 0;
    double f_        = 1;
    double value_    = 0;

    // parameters
    double Ts_      = 0.001;
    double A_start_ = 1;
    double A_end_   = 1;
    double freq_    = 1;
    double t_end_   = 10;

    // all spring values
    std::vector<int> readings_ = {0};

};

}

#endif
