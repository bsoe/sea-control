clc; clear; close all;

%% paramters

% hand fit the bode
plot_hand_fit = false;
wn   = 17.5;
zeta = 0.168;

% scaling = spring_position / spring_torque
scaling = 4.4;

% name to save figure and copy of data
export_name = 'FreqResponse_abc123';

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

Ts = data(2,1)-data(1,1);
istart = find(data(:,2)>0.1,1)-1; % start at freq = 0.1 rad/s;
iend   = size(data,1); %round(380/Ts);

time            = data(istart:iend,1);
frequency       = data(istart:iend,2);
motor_position  = data(istart:iend,3);
spring_position = data(istart:iend,4);
desired_torque  = data(istart:iend,5);
spring_torque   = data(istart:iend,6);
control_torque  = data(istart:iend,7);
friction_torque = data(istart:iend,8);
total_torque    = data(istart:iend,9);
actual_torque   = data(istart:iend,10);

N = length(time);

%% find offsets and scaling

figure
[mu,sigma] = MyNormalFit(motor_position,true);
motor_position  = motor_position - mu;
xlabel('motor position')
ylabel('# occurances')
title('histogram to find motor position offset')

figure
[mu,sigma] = MyNormalFit(spring_position,true);
spring_position  = spring_position - mu;
xlabel('spring position')
ylabel('# occurances')
title('histogram to find spring position offset')

%% plot time history
figure
subplot(2,1,1)
plot(time,desired_torque,...
     time,spring_torque,...
     time,control_torque);
ylabel('torque')
legend('desired','spring','control')
subplot(2,1,2)
plot(time,total_torque)
xlabel('time (s)')
ylabel('torque')
legend('total')


%% frequency response
Gest = etfe( iddata(spring_torque,desired_torque,Ts), [], N );

figure;
%bode(Gest,{2*pi*frequency(1),2*pi*frequency(end)})
bode(Gest,{2*pi*frequency(1),100})
hold on;

%% hand fit of frequency response
if plot_hand_fit
    s = tf('s');
    G = wn^2/(s^2+2*zeta*wn*s+wn^2);
    bode(G);
end

%% matlab tfest
temp = iddata(spring_torque,desired_torque,Ts);
sys = tfest(temp,2,0);
bode(sys);

[num,den] = tfdata(sys,'v');
wn = sqrt(den(3))
zeta = den(2)/(2*wn)
gain = num(3)/den(3)

%% legend
if plot_hand_fit
    legend('experimental','manual fit','tfest','Location','SouthWest')
else 
    legend('experimental','2nd-order fit','Location','SouthWest')
end
%% save
addpath('../export_fig')
dlmwrite(['data/' export_name '.txt'], data, 'delimiter','\t');
figure(4)
set(gcf, 'Color', 'w');
export_fig(['data/' export_name '.png'], '-m3');
