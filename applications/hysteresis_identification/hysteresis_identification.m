clc; clear; close all;

%% guess the scaling = spring_position/torque
scaling = 4.2;

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

time            = data(:,1);
motor_position  = data(:,2);
spring_position = data(:,3);
desired_torque  = data(:,4);
friction_torque = data(:,5);
total_torque    = data(:,6);
actual_torque   = data(:,7);

N = length(time);
Ts = time(2)-time(1);

%% find offsets and scaling
motor_position  = motor_position  - mean(motor_position);
spring_position = spring_position - mean(spring_position);

%% plot time history
figure
subplot(2,1,1)
plot(time,motor_position,...
     time,spring_position,...
     time,desired_torque)
xlabel('time (s)')
ylabel('position')
legend('motor position','spring position','desired torque')

subplot(2,1,2)
plot(time,friction_torque,...
     time,desired_torque,...
     time,total_torque)
xlabel('time (s)')
ylabel('torque')
legend('friction','desired','total')

figure
plot(motor_position,spring_position)
xlabel('motor position')
ylabel('spring position')
title('hysteresis in spring encoder')

figure
plot(desired_torque,spring_position,...
     total_torque,spring_position); hold on;
xlabel('torque')
ylabel('spring position')
legend('desired','total','Location','SouthEast')
title('hysteresis in torque output')

%% plot estimate of scaling
spring_max = max(spring_position);
spring_min = min(spring_position);

line([spring_min,spring_max]/scaling,...
     [spring_min,spring_max]);
 
xl = min(desired_torque);
xu = xl+(max(spring_position)-min(spring_position))/scaling;
x0 = xl-min(spring_position)/scaling;
line([xl,xu],...
     scaling*[xl-x0,xu-x0]);

xu = max(desired_torque);
xl = xu-(max(spring_position)-min(spring_position))/scaling;
x0 = xu-max(spring_position)/scaling;
line([xl,xu],...
     scaling*[xl-x0,xu-x0]);
 
 
 

