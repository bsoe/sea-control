Hysteresis Identification
=========================

This is an application for identifying hysteresis in the spring sensor, and hysteresis in the torque output:

1. Fix the load of the SEA
2. Build the example

        mkdir build
        cmake ..
        make

3. Start the ethercat master. Check that the slaves are listed.

        sudo ethercat_master start
        ethercat slaves

4. Test the example with 0 coulomb compensation. Stop the program with ctrl-c.

        friction.setCoulombNegative(0);
        friction.setCoulombPositive(0);

        sudo ./hysteresis_identification

7. Kill the ethercat master.

        ethercat_master stop
   
8. Run the matlab script "hysteresis_identification.m".



[SimpleCAT]:https://bitbucket.org/bsoe/simplecat
