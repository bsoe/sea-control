Applications
============

These are example applications, that show how to use this library to:

1. Collect data for plant identification
2. Design a controller for the plant

They were written for a setup with the following components

1. Motor controller with current control.
    [Elmo Gold Solo Whistle]

2. Encoder to measure motor velocity, and perform commutation if brushless.
    [Elmo Gold Solo Whistle], port A

3. Encoder to measure spring deflection
    [Elmo Gold Solo Whistle], port B

4. Optional encoder to measure output position
    Combo 1:
    
    * [Beckhoff EL5002] - SSI to EtherCAT
    * SSI encoder

    Combo 2:
    
    * [Beckhoff EL5101] - Quadrature to EtherCAT
    * Quadrature encoder 

5. EtherCAT master
    * [Etherlab] master with [SimpleCAT] wrapper

[Elmo Gold Solo Whistle]:http://www.elmomc.com/products/whistle-solo-servo-drive-gold.htm
[Beckhoff EL5002]:https://www.beckhoff.com/el5002/
[Beckhoff EL5101]:https://www.beckhoff.com/el5101/
[Etherlab]:https://etherlab.org/
