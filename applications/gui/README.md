GUI
===

This graphical user interface is for the torque control example. The following can be enabled:

* controller
* friction compensation
* lead compensator
* disturbance observer

Launch this application after starting the torque_control example. 

Note: The "gui.pro" is for editing "mainwindow.ui" in qtcreator only, and does not compile. See the below build instructions.
  

Build
=====

You may need to install qt5.

        sudo apt-get update
        sudo apt-get install qt5-default

First download [qcustomplot] to the folder "sea-control/applications/gui/qcustomplot". Then build.

        mkdir build & cd build
        cmake .. && make
        ./gui


[qcustomplot]:http://www.qcustomplot.com/
