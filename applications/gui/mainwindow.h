#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
//#include "qcustomplot/qcustomplot.h"
#include "../../include/sea/shm/SharedMemory.h"
#include "../../include/sea/shm/SharedMemoryStruct.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_checkBox_enable_clicked(bool checked);

    void on_checkBox_coulomb_clicked(bool checked);

    void on_checkBox_lead_clicked(bool checked);

    void on_checkBox_dob_clicked(bool checked);

	// plot
    void plotRealTimeData();

private:

    // user parameters
    const double min_update_period = 0.002; // seconds


    Ui::MainWindow *ui;
	
    // shared memory
    sea::SharedMemory<SharedMemoryStruct> shm;
    SharedMemoryStruct* shared_data = NULL;

    // timer for plotting
    int timerId;
    void timerEvent(QTimerEvent *event);

    // plotting
	QTimer dataTimer;
    double last_time = 0;
    double last_fps_time = 0;
    unsigned int frame_count = 0;
};

#endif // MAINWINDOW_H
