#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot/qcustomplot.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    timerId = startTimer(250);

    // open shared memory object
    shared_data = shm.openSharedMemory("sea-torque-control");

	// set default values from shared memory
    ui->checkBox_enable->setChecked(shared_data->enable);
    ui->checkBox_coulomb->setChecked(shared_data->enable_friction_compensation);
    ui->checkBox_lead->setChecked(shared_data->enable_lead_compensator);
    ui->checkBox_dob->setChecked(shared_data->enable_disturbance_observer);

	// customPlot
    ui->PlotTorque->addGraph(); // blue line
    ui->PlotTorque->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->PlotTorque->addGraph(); // red line
    ui->PlotTorque->graph(1)->setPen(QPen(QColor(255, 110, 40)));
    ui->PlotPosition->addGraph(); // blue line
    ui->PlotPosition->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->PlotPosition->addGraph(); // red line
    ui->PlotPosition->graph(1)->setPen(QPen(QColor(255, 110, 40)));
	 
	QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
	timeTicker->setTimeFormat("%h:%m:%s");
    ui->PlotTorque->xAxis->setTicker(timeTicker);
    ui->PlotTorque->axisRect()->setupFullAxesBox();
    ui->PlotTorque->yAxis->setRange(-1.2, 1.2);
    ui->PlotPosition->xAxis->setTicker(timeTicker);
    ui->PlotPosition->axisRect()->setupFullAxesBox();
    ui->PlotPosition->yAxis->setRange(-1.2, 1.2);
	 
	// make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->PlotTorque->xAxis, SIGNAL(rangeChanged(QCPRange)),
            ui->PlotTorque->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->PlotTorque->yAxis, SIGNAL(rangeChanged(QCPRange)),
            ui->PlotTorque->yAxis2, SLOT(setRange(QCPRange)));
    connect(ui->PlotPosition->xAxis, SIGNAL(rangeChanged(QCPRange)),
            ui->PlotPosition->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->PlotPosition->yAxis, SIGNAL(rangeChanged(QCPRange)),
            ui->PlotPosition->yAxis2, SLOT(setRange(QCPRange)));
	 
	// setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
	connect(&dataTimer, SIGNAL(timeout()), this, SLOT(plotRealTimeData()));
	dataTimer.start(0); // Interval 0 means to refresh as fast as possible
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    ui->lineEdit_spring_force->setText(QString::number(shared_data->spring_force));
}

void MainWindow::on_checkBox_enable_clicked(bool checked)
{
    shared_data->enable = checked;
	std::cout << "enable controller			   :" << checked << std::endl;
}

void MainWindow::on_checkBox_coulomb_clicked(bool checked)
{
    shared_data->enable_friction_compensation = checked;
	std::cout << "enable friction compensation :" << checked << std::endl;
}

void MainWindow::on_checkBox_lead_clicked(bool checked)
{
    shared_data->enable_lead_compensator = checked;
	std::cout << "enable lead compesator       :" << checked << std::endl;
}

void MainWindow::on_checkBox_dob_clicked(bool checked)
{
    shared_data->enable_disturbance_observer = checked;
	std::cout << "enable distrubance observer  :" << checked << std::endl;
}

void MainWindow::plotRealTimeData()
{
    double time = (double)shared_data->time.tv_sec + shared_data->time.tv_nsec/1000000000.0;

	// calculate two new data points:
	if (time-last_time > min_update_period) // at most add point every 2 ms
	{
	  // add data to lines:
      ui->PlotTorque  ->graph(0)->addData(time, shared_data->desired_force);
      ui->PlotTorque  ->graph(1)->addData(time, shared_data->spring_force);
      ui->PlotPosition->graph(0)->addData(time, shared_data->desired_position);
      ui->PlotPosition->graph(1)->addData(time, shared_data->output_position);
	  // rescale value (vertical) axis to fit the current data:
      ui->PlotTorque->graph(0)->rescaleValueAxis();
      ui->PlotTorque->graph(1)->rescaleValueAxis(true);
      ui->PlotPosition->graph(0)->rescaleValueAxis();
      ui->PlotPosition->graph(1)->rescaleValueAxis(true);
	  last_time = time;
	}
	// make key axis range scroll with the data (at a constant range size of 8):
    ui->PlotTorque->xAxis->setRange(time, 8, Qt::AlignRight);
    ui->PlotTorque->replot();
    ui->PlotPosition->xAxis->setRange(time, 8, Qt::AlignRight);
    ui->PlotPosition->replot();
	 
	// calculate frames per second:
	++frame_count;
	if (time-last_fps_time > 1) // average fps over 2 seconds
	{
	  ui->statusBar->showMessage(
		    QString("%1 FPS, Total Data points: %2")
		    .arg(frame_count/(time-last_fps_time), 0, 'f', 0)
            .arg(ui->PlotTorque->graph(0)->data()->size()
                +ui->PlotTorque->graph(1)->data()->size())
		    , 0);
	  last_fps_time = time;
	  frame_count = 0;
	}
}
