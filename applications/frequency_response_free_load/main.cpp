#include <sea/Chirp.h>
#include <sea/filter/LeadCompensator.h>
#include <sea/filter/LowPassFilter.h>
#include <sea/friction/CoulombFrictionCompensator.h>
#include <sea/DOBLeadController.h>

#include <sea/shm/SharedMemory.h>
#include <sea/shm/SharedMemoryStruct.h>

#include <simplecat/Master.h>
#include <simplecat/Elmo/Elmo.h>
#include <simplecat/Beckhoff/Beckhoff.h>

#ifdef SEA_USE_ATI_FORCE_SENSOR
	#include "cForceSensor.h"
	#include <unistd.h>
#endif

#include <iostream>
#include <fstream>
#include <cmath>

/****************************************************************************/

sea::DOBLeadController controller;
sea::LowPassFilter motor_filter;
sea::CoulombFrictionCompensator friction;
sea::Chirp chirp;
std::ofstream data;

simplecat::Master master;
simplecat::Elmo_GoldWhistle elmo_gw;
simplecat::Beckhoff_EK1100 bh_ek1100;
simplecat::Beckhoff_EL5101 bh_el5101;

std::ofstream data_file;

// torque constant, spring position / torque
const double SPRING_C2T  = 0.0027;
const double CURRENT_C2T = 0.012;
const double OUTPUT_C2P  = 2.0*M_PI/40000.0;
const double MOTOR_C2P   = 2.0*M_PI/ 4096.0;

// controller
const double wo = 17.5;     // rad/s
const double zo = 0.18;
const double wc = 12.0*wo;  // rad/s minimum 5*Wo
const double zc = 0.7071;
const double wq = 2.0*wo;     // rad/s
const double zq = 0.7071;
const double Ts = 0.001;    // s
const double min_output = -1000; // N m
const double max_output =  1000; // N m

// chirp setup
const double chirp_amplitude = 5.0;     // N m
const double chirp_f_min     = 0.001;   // Hz
const double chirp_f_max     = 10;      // Hz
const double chirp_duration  = 600;     // s
const double chirp_position_limit = M_PI/4.0;  // rad

// friction compensation
const double coulomb_deadzone =  8.0; // rad/s
const double coulomb_positive =  0.9;   // N m
const double coulomb_negative = -0.9;   // N m

#ifdef SEA_USE_ATI_FORCE_SENSOR
// ATI force sensor
std::string ATI_calibration_filename = "../../ATIForceSensor/calibrations/FT16213.cal";
cForceSensor ATI_sensor;
double ATI_force[3] = {0};
double ATI_torque[3] = {0};
#endif

// parameters
bool initialized = false;
int motor_count_offset  = 0;
int spring_count_offset = 0;
int output_count_offset = 0;
double desired_torque = 0;

// shared memory
SharedMemoryStruct *shared_data;

/****************************************************************************/

void ctrl_c_handler(int s)
{
    std::cout << "exiting" << std::endl;
    master.stop();
}

/****************************************************************************/

unsigned int loop_counter = 0;
void control_callback()
{
    if (!elmo_gw.initialized() || !bh_el5101.initialized()){ return; }

    if (!initialized){

        motor_count_offset  = elmo_gw.position_;
        spring_count_offset = elmo_gw.absolute_position_;
		output_count_offset = bh_el5101.counter();
        elmo_gw.max_torque_ = 1000;
        initialized = true;

        std::cout << "=====initialization=====\n";
        std::cout << "motor  count offset: " << motor_count_offset << '\n';
        std::cout << "spring count offset: " << spring_count_offset << '\n';
        std::cout << "output count offset: " << output_count_offset << '\n';
        std::cout << "=====initialization=====\n";
    }

    // position and velocity
    int spring_count = elmo_gw.absolute_position_ - spring_count_offset;
    int motor_count  = elmo_gw.position_          - motor_count_offset;
    int output_count = bh_el5101.counter()        - output_count_offset;

    // conversion to SI
    double spring_torque   =  SPRING_C2T * spring_count;
    double motor_position  =  MOTOR_C2P  * motor_count;
    double output_position = -OUTPUT_C2P * output_count;

    // filtered position velocity
    motor_filter.update(motor_position);
    double motor_filtered_position = motor_filter.out(0);
    double motor_filtered_velocity = (motor_filter.out(0) - motor_filter.out(1))/Ts;

    // friction compensation
    double friction_torque = friction.friction(motor_filtered_velocity);
    
    // frequency response for free load
    desired_torque    = chirp.step();
    desired_torque   -= chirp_amplitude*(output_position*fabs(output_position))/ 
                                        (chirp_position_limit*chirp_position_limit);
    double frequency  = chirp.frequency();

    // controller
    double controller_torque = controller.update(desired_torque,spring_torque);
    double error_torque = desired_torque - spring_torque;

    // total torque
    elmo_gw.target_torque_ = 0;
    elmo_gw.target_torque_ += std::round(friction_torque/CURRENT_C2T);
    elmo_gw.target_torque_ += std::round(controller_torque/CURRENT_C2T);

	// update the force sensor
#ifdef SEA_USE_ATI_FORCE_SENSOR
    ATI_sensor.AcquireFTData();
    ATI_sensor.GetForceReading(ATI_force);
    ATI_sensor.GetTorqueReading(ATI_torque);
#endif

    // update the shared data from the new values
	shared_data->time             = controller.time();
	shared_data->desired_force    = desired_torque;
    shared_data->spring_force     = spring_torque;
	shared_data->command_force    = CURRENT_C2T*elmo_gw.target_torque_;
    shared_data->desired_position = 0;
    shared_data->output_position  = output_position;

	// update what part of the controller is enabled from shared memory
	bool enable_temp 		  = shared_data->enable;
	bool enable_lead_temp     = shared_data->enable_lead_compensator;
	bool enable_dob_temp      = shared_data->enable_disturbance_observer;
	bool enable_friction_temp = shared_data->enable_friction_compensation;
	if (enable_temp != controller.enabled()){
		std::cout << "enable controller            : " << enable_temp << '\n';
		controller.enable(enable_temp);
	}
	if (enable_lead_temp != controller.enabledLead()){
		std::cout << "enable lead compensator      : " << enable_lead_temp << '\n';
		controller.enableLead(enable_lead_temp);
	}
	if (enable_dob_temp != controller.enabledDOB()){
		std::cout << "enable disturbance observer  : " << enable_dob_temp << '\n';
		controller.enableDOB(enable_dob_temp);
	}
	if (enable_friction_temp != friction.enabled()){
		std::cout << "enable friction compensation : " << enable_friction_temp << '\n';
		friction.enable(enable_friction_temp);
	}

    // write data to file
    double time = Ts*master.elapsedCycles();
    data_file 	<< time << '\t'
              	<< frequency << '\t'
                << output_position << '\t'
				<< motor_position << '\t'
				//<< spring_count << '\t'
				<< desired_torque << '\t'
				<< spring_torque << '\t'
				<< controller_torque << '\t'
				//<< friction_torque << '\t'
				<< elmo_gw.target_torque_ << '\t'
				<< elmo_gw.torque_
#ifndef SEA_USE_ATI_FORCE_SENSOR
				<< '\n';
#else
				<< '\t'
				<< ATI_force[0] << '\t'
				<< ATI_force[1] << '\t'
				<< ATI_force[2] << '\t'
				<< ATI_torque[0] << '\t'
				<< ATI_torque[1] << '\t'
				<< ATI_torque[2] << '\n';
#endif

    if (loop_counter %500==0){

        std::cout << "===========================\n";
        std::cout << std::dec;
        std::cout << "time            : " << time << '\n';
        std::cout << "frequency       : " << frequency << '\n';
        std::cout << "output   pos    : " << output_position << '\n';
        std::cout << "motor    pos    : " << motor_count << '\n';
        //std::cout << "spring   count  : " << spring_count << '\n';  
        std::cout << "desired  torque : " << desired_torque << '\n';
        std::cout << "spring   torque : " << spring_torque << '\n';
        std::cout << "control  torque : " << controller_torque << '\n';
        std::cout << "friction torque : " << friction_torque << '\n';  
        std::cout << "total    torque : " << elmo_gw.target_torque_ << '\n';
        std::cout << "actual   torque : " << elmo_gw.torque_ << '\n';
#ifdef SEA_USE_ATI_FORCE_SENSOR
        std::cout << "ATI      force  : " << ATI_force[0] << '\t'
									      << ATI_force[1] << '\t'
										  << ATI_force[2] << '\n';
        std::cout << "ATI      torque : " << ATI_torque[0] << '\t'
									      << ATI_torque[1] << '\t'
										  << ATI_torque[2] << '\n';
#endif

    }

    ++loop_counter;

    if (chirp.time()>=chirp.duration()){
        master.stop();
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{
    // chirp setup
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(chirp_amplitude);
    chirp.setFrequencyRange(chirp_f_min, chirp_f_max);
    chirp.setDuration(chirp_duration);

    // controller
	std::cout << "Initializing controller" << std::endl;
    controller.setSampleTime(Ts);
    controller.setOpenLoopPlant(wo,zo);
    controller.setClosedLoopPlant(wc,zc);
    controller.setDOB(wq,zq);
    controller.enableLead(false);
    controller.enableDOB(false);
    controller.enable(true);
    controller.setLimits(min_output,max_output);

    // velocity filter
    motor_filter.setButterworth(314,Ts);;

    // friction
    friction.setBuffer(coulomb_deadzone);
    friction.setCoulombPositive(coulomb_positive);
    friction.setCoulombNegative(coulomb_negative);

    // create shared memory object
	std::cout << "Initializing shared memory" << std::endl;
    sea::SharedMemory<SharedMemoryStruct> shm;
    shared_data = shm.createSharedMemory("sea-torque-control");
	shared_data->enable                       = controller.enabled();
    shared_data->enable_lead_compensator      = controller.enabledLead();
    shared_data->enable_disturbance_observer  = controller.enabledDOB();
    shared_data->enable_friction_compensation = friction.enabled();

#ifdef SEA_USE_ATI_FORCE_SENSOR
	// Initialize the force sensor
	std::cout << "Initializing force sensor" << std::endl;
    ATI_sensor.Set_Calibration_File_Loc(ATI_calibration_filename);
    ATI_sensor.Initialize_Force_Sensor("/dev/comedi0");
    usleep(2000000);
    ATI_sensor.Zero_Force_Sensor();
#endif

    // data file
    data_file.open("data.txt");

    // ethercat master
    master.setCtrlCHandler(ctrl_c_handler);
    master.addSlave(0,0,&elmo_gw);
    master.addSlave(0,1,&bh_ek1100);
    master.addSlave(0,2,&bh_el5101);

    elmo_gw.max_torque_ = 1000;

    //master.setThreadHighPriority();
    master.setThreadRealTime();
    master.activate();

    // run main loop
    master.run(control_callback, 1.0/Ts);

    // close data file
    data.close();

    std::cout << "run time : " << master.elapsedTime() << std::endl;
    std::cout << "updates  : " << master.elapsedCycles() << std::endl;
    std::cout << "frequency: " << master.elapsedCycles()/master.elapsedTime() << std::endl;

    return 0;
}

/****************************************************************************/
