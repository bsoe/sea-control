clc; clear; close all;


export_name = 'FreqResponse_comparison';
data_files = {'data/FreqResponse_air.txt',...
              'data/FreqResponse_water.txt'};
          
legend_names = {'air','water'};

for i = 1:length(data_files)
        
    %% import data
    data_file = data_files{i};
    data = importdata(data_file);

    Ts = data(2,1)-data(1,1);
    istart = 1;             %find(data(:,2)>0.4,1)-1; % start at freq = 0.1 rad/s;
    iend   = size(data,1);

    time             = data(istart:iend,1);
    frequency        = data(istart:iend,2);
    output_position  = data(istart:iend,3);
    motor_position   = data(istart:iend,4);
    %spring_count  = data(istart:iend,3);
    desired_torque   = data(istart:iend,5);
    spring_torque    = data(istart:iend,6);
    controller_torque = data(istart:iend,7);
    %friction_torque  = data(istart:iend,8);
    total_torque     = data(istart:iend,8);
    actual_torque    = data(istart:iend,9);
    %{
    ATI_force_x      = data(istart:iend,10);
    ATI_force_y      = data(istart:iend,11);
    ATI_force_z      = data(istart:iend,12);
    ATI_torque_x     = data(istart:iend,13);
    ATI_torque_y     = data(istart:iend,14);
    ATI_torque_z     = data(istart:iend,15);
    %}

    N = length(time);

    %% find offsets and scaling

    [mu,sigma] = MyNormalFit(motor_position,false);
    motor_position  = motor_position - mu;

    [mu,sigma] = MyNormalFit(spring_torque,false);
    spring_torque  = spring_torque - mu;

    %% frequency response
    Gest = etfe( iddata(spring_torque,desired_torque,Ts), [], N );
    bode(Gest,{3.3,61.3})
    hold on;

    %% matlab tfest
    %{
    wn = 17.5;
    zeta = 0.18;

    k  = 216;
    mo = k/wn^2
    bo = 2*zeta*sqrt(mo*k)
    ml = 0.1;   %22;
    bl = 0.001; %45;

    s = tf('s');
    alpha = (ml*s^2+bl*s)/(ml*s^2+bl*s+k);
    Gmodel = k*alpha/(mo*s^2+bo*s+k*alpha);

    bode(Gmodel);
    hold on;
    %}
end

%% legend
legend(legend_names,'Location','SouthWest')


%% save
addpath('../export_fig')
set(gcf, 'Color', 'w');
export_fig(['data/' export_name '.png'], '-m3');
