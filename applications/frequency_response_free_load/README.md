Frequency Response
==================

This is an application for system identification of an SEA, by finding the open loop frequency response when the load is fixed. The recommended steps are:

1. Fix the load of the SEA
2. Build the example

        mkdir build
        cmake ..
        make

3. Start the ethercat master. Check that the slaves are listed.

        sudo ethercat_master start
        ethercat slaves

4. Set the values for the following parameters

    * torque constant
    * controller (note that wo,zo are found from "frequency_response_open_loop" example)
    * coulomb friction

5. Run the program until completion. A "data.txt" should be generated.

        chirp.amplitude_ = ?

6. Kill the ethercat master.

        ethercat_master stop
   
7. Open the matlab script "frequency_response.m". Set the output file name, for the generated plot and data to be saved. Then run the script. The script will fit a 2nd order system to the experimental bode, and print the wn and zeta. You can also manually fit the second order system.



[SimpleCAT]:https://bitbucket.org/bsoe/simplecat
