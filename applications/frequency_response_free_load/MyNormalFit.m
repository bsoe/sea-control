function [mu,sigma] = MyNormalFit(data,plot_yn)

[count,edges] = histcounts(data);
bins = (edges(2:end)+edges(1:end-1))/2;

f1 = fit(bins',count','gauss1'); %, 'Exclude', x<800);

mu = f1.b1;
sigma = sqrt(f1.c1/2);

if nargin>1 & plot_yn
    histogram(data); hold on;
    plot(f1)
end

end