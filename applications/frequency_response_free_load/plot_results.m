clc; clear; close all;

plot_hand_fit = false;
export_name = 'FreqResponse_air';

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);

Ts = data(2,1)-data(1,1);
istart = 1;             %find(data(:,2)>0.4,1)-1; % start at freq = 0.1 rad/s;
iend   = size(data,1);

time             = data(istart:iend,1);
frequency        = data(istart:iend,2);
output_position  = data(istart:iend,3);
motor_position   = data(istart:iend,4);
%spring_count  = data(istart:iend,3);
desired_torque   = data(istart:iend,5);
spring_torque    = data(istart:iend,6);
controller_torque = data(istart:iend,7);
%friction_torque  = data(istart:iend,8);
total_torque     = data(istart:iend,8);
actual_torque    = data(istart:iend,9);
%{
ATI_force_x      = data(istart:iend,10);
ATI_force_y      = data(istart:iend,11);
ATI_force_z      = data(istart:iend,12);
ATI_torque_x     = data(istart:iend,13);
ATI_torque_y     = data(istart:iend,14);
ATI_torque_z     = data(istart:iend,15);
%}

N = length(time);

%% find offsets and scaling

figure
[mu,sigma] = MyNormalFit(motor_position,true);
motor_position  = motor_position - mu;
xlabel('motor position')
ylabel('# occurances')
title('histogram to find motor position offset')

figure
[mu,sigma] = MyNormalFit(spring_torque,true);
spring_torque  = spring_torque - mu;
xlabel('spring position')
ylabel('# occurances')
title('histogram to find spring position offset')

%% plot time history
figure
subplot(2,1,1)
plot(time,desired_torque,...
     time,spring_torque,...
     time,controller_torque);
ylabel('torque')
legend('desired','spring','control')
subplot(2,1,2)
plot(time,total_torque)
xlabel('time (s)')
ylabel('torque')
legend('total')


%% frequency response
Gest = etfe( iddata(spring_torque,desired_torque,Ts), [], N );

figure;
%bode(Gest,{2*pi*frequency(1),2*pi*frequency(end)})
%bode(Gest,{2*pi*frequency(1),100})
bode(Gest,{1.5,62})
hold on;

%% matlab tfest
wn = 17.5;
zeta = 0.18;

k  = 216;
mo = k/wn^2
bo = 2*zeta*sqrt(mo*k)
ml = 0.15;   %22;
bl = 0.001; %45;

s = tf('s');
alpha = (ml*s^2+bl*s)/(ml*s^2+bl*s+k);
Gmodel = k*alpha/(mo*s^2+bo*s+k*alpha);

bode(Gmodel)

%% legend
legend('experimental','model','Location','SouthWest')

%% save
addpath('../export_fig')
dlmwrite(['data/' export_name '.txt'], data, 'delimiter','\t');
figure(4)
set(gcf, 'Color', 'w');
export_fig(['data/' export_name '.png'], '-m3');
