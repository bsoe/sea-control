clc; clear; close all;

plot_hand_fit = false;
export_name = 'position_tracking_air_chirp';

%% import data
data_file = 'build/data.txt';
data = importdata(data_file);
%data(:,1) = data([0:0.001:0.001*(length(data(:,1))-1)]);

Ts = data(2,1)-data(1,1);
istart = find(data(:,2)>0.1,1)-1; % start at freq = 0.1 rad/s;
iend   = size(data,1);

%istart = find(data(:,1)>3.0,1)-1;
%iend   = find(data(:,1)>5.0,1)-1;

time             = data(istart:iend,1);
frequency        = data(istart:iend,2);
desired_position = data(istart:iend,3);
output_position  = data(istart:iend,4);
motor_position   = data(istart:iend,5);
desired_torque   = data(istart:iend,6);
spring_torque    = data(istart:iend,7);
controller_torque = data(istart:iend,8);
%friction_torque  = data(istart:iend,8);
total_torque     = data(istart:iend,9);
actual_torque    = data(istart:iend,10);
output_filter_pos = data(istart:iend,11);
output_filter_vel = data(istart:iend,12);
output_filter_acc = data(istart:iend,13);
position_error    = data(istart:iend,14);
velocity_error    = data(istart:iend,15);

N = length(time);

%% plot
figure
plot(time, desired_position,...
     time, output_position);
legend('desired','measured')
legend('desired','measured')
xlabel('time (s)')
ylabel('position (rad)')

figure
subplot(2,1,1)
plot(time, controller_torque);
subplot(2,1,2)
plot(time, position_error); hold on;
plot(time, velocity_error);
ylabel('error')
legend('position','velocity');

%% plot
figure
subplot(3,1,1)
plot(time, desired_position,...
     time, output_filter_pos);
legend('desired','measured')
ylabel('position (rad)')
subplot(3,1,2)
plot(time, output_filter_vel);
ylabel('velocity (rad/s)')
subplot(3,1,3)
plot(time, output_filter_acc);
ylabel('acceleration (rad/s^2)')
xlabel('time (s)')

%% try different adaptive filters
addpath('../../include/sea/adaptpolyfilter')

% paramters
APF = AdaptPolyFilter;
APF.setMaxWindowSize(50);
APF.sampling_freq   = 1.0/Ts;
APF.noise_stddev    = 1.5*(2.0*pi/40000.0);

% run simulation
x_aw = zeros(1,N);
v_aw = zeros(1,N);
a_aw = zeros(1,N);
aw_win_size = zeros(1,N);
for i = 1:length(output_position)

    APF.update(output_position(i));

    x_aw(i) = APF.position;
    v_aw(i) = APF.velocity;
    a_aw(i) = APF.acceleration;
    aw_win_size(i) = APF.window_size;
    
end

% plot
figure;

subplot(4,1,1); hold on;
plot(time,output_position);
stairs(time,x_aw);
xlim([time(1) time(end)]);
ylabel('position')
legend('real','filtered','Location','NorthWest');

subplot(4,1,2); hold on;
stairs(time,v_aw);
xlim([time(1) time(end)]);
ylabel('velocity')

subplot(4,1,3); hold on;
stairs(time,a_aw);
xlim([time(1) time(end)]);
ylabel('acceleration')

subplot(4,1,4);
stairs(time, aw_win_size);
xlim([time(1) time(end)]);
ylabel('window size')
xlabel('time (s)')


%% plot torque
figure
plot(time, desired_torque,...
     time, spring_torque)
legend('desired','measured')
xlabel('time (s)')
ylabel('torque (rad)')

%% least squares
% mass*acc + damp*vel = torque
% [acc vel]*[mass damp]' = torque
% [mass damp]' = ([acc vel]'*[acc vel])^-1*[acc vel]'*torque

A = [output_filter_acc output_filter_vel];
x = (A'*A)^-1*A'*spring_torque;

est_mass = x(1)
est_damp = x(2)

%% save
addpath('../export_fig')
dlmwrite(['data/' export_name '.txt'], data, 'delimiter','\t');
figure(1)
set(gcf, 'Color', 'w');
export_fig(['data/' export_name '.png'], '-m3');








