#include <sea/filter/LowPassFilter.h>
#include <sea/friction/CoulombFrictionCompensator.h>
#include <sea/Chirp.h>

#include <simplecat/Master.h>
#include <simplecat/Elmo/Elmo.h>

#include <iostream>
#include <fstream>
#include <cmath>

/****************************************************************************/

sea::LowPassFilter motor_filter;
sea::CoulombFrictionCompensator friction;
sea::Chirp chirp;
std::ofstream data;

simplecat::Master master;
simplecat::Elmo_GoldWhistle elmo_gw;

std::ofstream data_file;

// torque amplitude
const double coulomb_deadzone =   5;
const double coulomb_positive =  75;
const double coulomb_negative = -75;

// control frequency
const double Ts = 0.001;

// chirp setup
const double chirp_amplitude = 250;
const double chirp_f_min     = 0.005;
const double chirp_f_max     = 50;
const double chirp_duration  = 600;

// parameters
bool initialized = false;
int motor_position_offset  = 0;
int spring_position_offset = 0;
int output_position_offset = 0;
double desired_torque = 0;


/****************************************************************************/

void ctrl_c_handler(int s)
{
    std::cout << "exiting" << std::endl;
    master.stop();
}

/****************************************************************************/

unsigned int loop_counter = 0;
void control_callback()
{
    if (!elmo_gw.initialized()){ return; }

    if (!initialized){
        motor_position_offset  = elmo_gw.position_;
        spring_position_offset = elmo_gw.absolute_position_;
        elmo_gw.max_torque_ = 1000;
        initialized = true;

        std::cout << "=====initialization=====\n";
        std::cout << "motor position offset:  " << motor_position_offset << '\n';
        std::cout << "spring_position offset: " << spring_position_offset << '\n';
        std::cout << "output_position offset: " << output_position_offset << '\n';
        std::cout << "=====initialization=====\n";
    }

    // position and velocity
    int motor_position = elmo_gw.position_ - motor_position_offset;
    int spring_position = elmo_gw.absolute_position_ - spring_position_offset;

    // friction compensation
    motor_filter.update(motor_position);
    double motor_filtered_position = motor_filter.out(0);
    double motor_filtered_velocity = motor_filter.out(0) - motor_filter.out(1);
    double friction_torque = friction.friction(motor_filtered_velocity);

    // chirp
    double desired_torque = chirp.step();
    double frequency = chirp.frequency();

    // total torque
    elmo_gw.target_torque_ = 0;
    elmo_gw.target_torque_ += std::round(friction_torque);
    elmo_gw.target_torque_ += std::round(desired_torque);

    // write data to file
    double time = Ts*master.elapsedCycles();
    data_file << time << '\t'
              << frequency << '\t'
              << motor_position << '\t'
              << spring_position << '\t'
              << desired_torque << '\t'
              << friction_torque << '\t'
              << elmo_gw.target_torque_ << '\t'
              << elmo_gw.torque_ << '\n';

    if (loop_counter %500==0){

        std::cout << "===========================\n";
        std::cout << std::dec;
        std::cout << "time            : " << time << '\n';
        std::cout << "frequency       : " << frequency << '\n';
        std::cout << "motor    pos    : " << motor_position << '\n';
        std::cout << "spring   pos    : " << spring_position << '\n';
        std::cout << "motor    vel    : " << motor_filtered_velocity << '\n';
        std::cout << "friction torque : " << friction_torque << '\n';        
        std::cout << "desired  torque : " << desired_torque << '\n';
        std::cout << "total    torque : " << elmo_gw.target_torque_ << '\n';
        std::cout << "actual   torque : " << elmo_gw.torque_ << '\n';
    }

    ++loop_counter;

    if (chirp.time()>=chirp.duration()){
        master.stop();
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{
    // chirp setup
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(chirp_amplitude);
    chirp.setFrequencyRange(chirp_f_min, chirp_f_max);
    chirp.setDuration(chirp_duration);

    // velocity filter
    motor_filter.setButterworth(314,Ts);

    // friction
    friction.setBuffer(coulomb_deadzone);
    friction.setCoulombPositive(coulomb_positive);
    friction.setCoulombNegative(coulomb_negative);

    // data file
    data_file.open("data.txt");

    // ethercat master
    master.setCtrlCHandler(ctrl_c_handler);
    master.addSlave(0,0,&elmo_gw);

    //master.setThreadHighPriority();
    master.setThreadRealTime();
    master.activate();

    // run main loop
    master.run(control_callback, 1.0/Ts);

    // close data file
    data.close();

    std::cout << "run time : " << master.elapsedTime() << std::endl;
    std::cout << "updates  : " << master.elapsedCycles() << std::endl;
    std::cout << "frequency: " << master.elapsedCycles()/master.elapsedTime() << std::endl;

    return 0;
}

/****************************************************************************/
