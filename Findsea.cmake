# ====== USER CONFIGURATION ======

# copy these lines into your CMakeLists.txt, above find_package(sea)
# with option set as ON or OFF
option(SEA_USE_SHM    "SEA_USE_SHM"     ON)
option(SEA_USE_SIM    "SEA_USE_SIM"     ON)

# ====== FIND AND LINK LIBRARIES ======

# SEA
set(SEA_DIR ${CMAKE_CURRENT_LIST_DIR})
if(NOT TARGET sea)
    # UNCOMMENT THIS IF YOU CHANGE FROM HEADERS ONLY TO A LIBRARY
    #find_library( SEA_LIBRARY NAMES sea PATHS ${SEA_DIR}/lib/ )
endif()

# SHARED MEMORY
if (SEA_USE_SHM OR SEA_USE_SIM)
    find_package(Boost 1.54.0 REQUIRED)
    set(SEA_SHM_LIBRARIES  -lrt -pthread)
endif(SEA_USE_SHM OR SEA_USE_SIM)

# create headers list
set(SEA_INCLUDE_DIRS    ${SEA_DIR}/include
                        ${Boost_INCLUDE_DIRS}
)

# create libraries list
set(SEA_LIBRARIES       ${SEA_LIBRARY}
						${Boost_LIBRARIES}
                        ${SEA_SHM_LIBRARIES}
)

#message("SEA HEADERS:\n" ${SEA_INCLUDE_DIRS})
#message("SEA LIBS:\n" ${SEA_LIBRARIES})
