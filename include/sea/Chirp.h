/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_CHIRP_H_
#define SEA_CHIRP_H_

#include <cmath>

namespace sea {

/** \brief Chirp base class
 * sweeping sine wave (chirp)
 * linear increasing frequency  
 */
class Chirp
{
public:

    Chirp(){}
    virtual ~Chirp(){}

    /** \brief Step the chirp by Ts.
     * \return The new value
     */
    virtual double step()
    {
        t_ = Ts_*((double)i_);
        f_ = f_min_ + (f_max_ - f_min_)*(t_/t_end_);

        // sweeping sine wave (chirp)
        // linear increasing frequency    
        double phi       = f_min_*t_ + 0.5*(f_max_ - f_min_)*(t_*t_/t_end_);
        
        // rise and fall time to smooth spectral ripple
        double amplitude = amplitude_;
        if (t_ > t_end_) {
            amplitude = 0; 
        } else if (t_ < t_rise_) {
            amplitude = amplitude_*(t_/t_rise_);
        } else if (t_ > (t_end_-t_fall_)) {
            amplitude = amplitude_*((t_end_-t_)/t_fall_);
        } else {
            amplitude = amplitude_;
        }
        value_ = amplitude*sin(2*M_PI*phi);

        ++i_;

        return value_;
    }

    /** \brief Reset the time to some point in the chirp */
    virtual void resetTime(double time = 0){
        i_ = std::round(time/Ts_);
        t_ = Ts_*((double)i_);
    }

    /** \brief Update the Chirp
     * \param time Time */
    virtual const double& value(){
        return value_;
    }

    virtual const double& frequency(){
        return f_;
    }

    virtual const double& time(){
        return t_;
    }

    virtual const double& duration(){
        return t_end_;
    }
    
    virtual const unsigned long& count(){
        return i_;
    }

    virtual void setSamplingTime(double Ts){
        Ts_ = Ts;
        resetTime(t_);
    }

    virtual void setAmplitude(double amplitude){
        amplitude_ = amplitude;
    }

    virtual void setFrequencyRange(double f_min, double f_max){
        f_min_ = f_min;
        f_max_ = f_max;
    }

    virtual void setDuration(double end_time, double rise_time=0, double fall_time=0){
        t_end_  = end_time;
        t_rise_ = rise_time;
        t_fall_ = fall_time;
    }

protected:

    unsigned long i_ = 0;
    double t_  = 0;
    double f_  = 1;
    double value_ = 0;

    // parameters
    double Ts_ = 0.001;
    double amplitude_     = 1;
    double f_min_         = 0.1;
    double f_max_         = 10;
    double t_end_  = 10;
    double t_rise_ = 0;
    double t_fall_ = 0;

};

}

#endif
