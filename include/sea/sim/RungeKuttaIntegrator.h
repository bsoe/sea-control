/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_RUNGEKUTTAINTEGRATOR_H_
#define SEA_RUNGEKUTTAINTEGRATOR_H_

#include <sea/sim/Integrator.h>
#include <sea/sim/SEASimulation.h>

#include <boost/numeric/odeint.hpp>
#include <vector>

#define ode boost::numeric::odeint

namespace sea {

/** \brief Runge-Kutta Integrator class
 *
 * http://www.boost.org/doc/libs/1_57_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/getting_started/short_example.html
 */
class RungeKuttaIntegrator : public Integrator
{
public:

    RungeKuttaIntegrator()
    {
        derived_ = this;
    }
    virtual ~RungeKuttaIntegrator(){}

    /** \brief Step the RungeKuttaIntegrator by dt */
    virtual void step()
    {
        num_substeps_ = ode::integrate( *this, x_ , 0.0 , dt_ , 0.1*dt_ );
        t_ += dt_;
    }

    /** \brief Number of substeps take in the last time step */
    int numberOfSubsteps(){
        return num_substeps_;
    }

    /** \brief Calculate the derivative of the state vector */
    void operator() ( const std::vector<double> &x , std::vector<double> &dxdt , const double t )
    {
        // If we use this->calculateDerivative,
        // then Integrator::calculateDerivative is called instead of the derived class implementation.
        // This may be because integrate is calling this function in a constructor.
        // By saving a copy of the this pointer, we can work around this issue.
        derived_->calculateDerivative(x,dxdt,t);
    }

protected:

    /// number of steps
    size_t num_substeps_ = 0;

    /// pointer to derived class.
    /// needed because ode::integrate will not use
    /// the overloaded function from derived class on this pointer
    RungeKuttaIntegrator* derived_ = NULL;

};

}

#endif
