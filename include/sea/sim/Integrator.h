/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_INTEGRATOR_H_
#define SEA_INTEGRATOR_H_

#include <vector>
#include <stdexcept>
#include <iostream>

namespace sea {


/** \brief Integrator base class
 */
class Integrator
{
public:

    Integrator(){}
    virtual ~Integrator(){}

    /** \brief set the time */
    virtual void setTime(const double& t){
        t_ = t;
    }

    /** \brief get the time */
    virtual const double& time(){
        return t_;
    }

    /** \brief set the time step */
    virtual void setTimeStep(const double& dt){
        dt_ = dt;
    }

    /** \brief get the time step */
    virtual const double& timeStep(){
        return dt_;
    }

    /** \brief Set the initial condition */
    virtual void setInitialCondition(const std::vector<double>& x0)
    {
        x_ = x0;
    }

    /** \brief Get the state */
    virtual const std::vector<double> x()
    {
        return x_;
    }

    /** \brief Get the state by index */
    virtual const double& x(int i)
    {
        return x_[i];
    }

    /** \brief Get the state */
    virtual const std::vector<double> dxdt()
    {
        return dxdt_;
    }

    /** \brief Get the state by index */
    virtual const double& dxdt(int i)
    {
        return dxdt_[i];
    }

    /** \brief Step the integrator by dt */
    virtual void step() = 0;

    /** \brief Calculate the derivative of the state vector */
    virtual void calculateDerivative (const std::vector<double> &x , std::vector<double> &dxdt , const double t)
    {
        throw std::runtime_error("Integrator. The derivative function should be overloaded if used.");
    }

protected:

    //// current time
    double t_ = 0;

    /// time step
    double dt_ = 0.001;

    /// state vector
    std::vector<double> x_;

    /// derivative of the state vector
    std::vector<double> dxdt_;

};

}

#endif
