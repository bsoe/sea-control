/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_BOUNCINGBALLSIMULATION_H_
#define SEA_BOUNCINGBALLSIMULATION_H_

#include <sea/sim/Integrator.h>

#include <vector>

namespace sea {

/** \brief Simulation base class
 */
template<class T>
class BouncingBallSimulation : public T
{
public:

    BouncingBallSimulation()
    {
        static_assert(std::is_base_of<Integrator, T>::value, "T must derive from Integrator");
    }
    virtual ~BouncingBallSimulation(){}

    /** \brief Calculate the derivative of the state vector */
    /** \brief Calculate the derivative of the state vector */
    virtual void calculateDerivative (const std::vector<double> &x , std::vector<double> &dxdt , const double t)
    {
        dxdt.resize(x.size());

        x_  = x[0];
        dx_ = x[1];

        if (x_>0){
            fk_ = k_*x_;
        } else {
            fk_ = 0;
        }

        dxdt[0] = dx_;
        dxdt[1] = (-fk_ - b_*dx_ + m_*g_ + d_)/m_;
    }

    /** Force in the spring */
    virtual const double& springForce()
    {
        return fk_;
    }

    /** Force on the link */
    virtual const double& disturbance()
    {
        return d_;
    }

    /// motor mass
    double m_ = 1;

    /// motor damping
    double b_ = 0;

    /// spring constant
    double k_ = 1;

    /// gravity acceleration
    double g_ = 10.0;

    /// disturbance force
    double d_ = 0;

protected:

    /// for debugging inheritance
    virtual void blah(){std::cout << "SEASim" << std::endl;}

    double x_  = 0;
    double dx_ = 0;
    double fk_ = 0;

};

}

#endif
