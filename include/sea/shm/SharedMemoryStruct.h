#ifndef SEA_SHM_SHAREDMEMORYSTRUCT_H_
#define SEA_SHM_SHAREDMEMORYSTRUCT_H_

// for mutex
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>

// for time
#include <time.h>

struct SharedMemoryStruct
{
	struct timespec time;

    //int32_t spring_force_count  = 0;
    //int32_t command_force_count = 0;
    //int32_t desired_force_count = 0; 
    //int32_t output_position_count = 0;

    double  spring_force        = 0;
    double  command_force       = 0;
    double  desired_force       = 0;
    double  output_position     = 0;
    double  desired_position    = 0;

    bool enable                       = false;
    bool enable_friction_compensation = false;
    bool enable_lead_compensator      = false;
    bool enable_disturbance_observer  = false;
    
    //Mutex to protect access to the queue
    boost::interprocess::interprocess_mutex mutex;
};

#endif
