/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_SHM_SHAREDMEMORY_H_
#define SEA_SHM_SHAREDMEMORY_H_

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <string>

namespace sea {

#define SEA_MUTEX boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex>

template <class T>
class SharedMemory
{
public:
    SharedMemory(){}

    ~SharedMemory()
    {
        boost::interprocess::shared_memory_object::remove(name_.c_str());
    }

    /** creates the shared memory struct */
    T* createSharedMemory(std::string name)
    {
        name_ = name;

        //Remove shared memory
        boost::interprocess::shared_memory_object::remove(name_.c_str());

        //Create a shared memory object.
        shm_ = boost::interprocess::shared_memory_object
            (boost::interprocess::create_only   //only create
            ,name_.c_str()                      //name
            ,boost::interprocess::read_write    //read-write mode
            );

        //Set size
        shm_.truncate(sizeof(T));

        //Map the whole shared memory in this process
        region_ = boost::interprocess::mapped_region
            (shm_                               //What to map
            ,boost::interprocess::read_write    //Map it as read-write
            );

        //Get the address of the mapped region
        addr_ = region_.get_address();

        //Construct the shared structure in memory
        data_ = new (addr_) T;
        return data_;
    }

    /** open the shared memory struct */
    T* openSharedMemory(std::string name)
    {
        name_ = name;

        //Open the shared memory object.
        shm_ = boost::interprocess::shared_memory_object
            (boost::interprocess::open_only     //only create
            ,name_.c_str()                      //name
            ,boost::interprocess::read_write    //read-write mode
            );

        //Map the whole shared memory in this process
        region_ = boost::interprocess::mapped_region
            (shm_                               //What to map
            ,boost::interprocess::read_write    //Map it as read-write
            );

        //Get the address of the mapped region
        addr_ = region_.get_address();

        //Construct the shared structure in memory
        data_ = static_cast<T*>(addr_);
        return data_;
    }

private:
    std::string name_;
    boost::interprocess::shared_memory_object shm_;
    boost::interprocess::mapped_region region_;
    void * addr_ = NULL;
    T* data_ = NULL;
};

}

#endif // SHAREDMEMORY_H
