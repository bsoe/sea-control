/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_LOWPASSFILTER_H_
#define SEA_LOWPASSFILTER_H_

#include "DiscreteFilter.h"
#include <cmath>

namespace sea {

/**
 * \brief 2nd order low pass filter.
 *
 * \f$ \frac{\omega_n^2}{s^2+2 \zeta \omega_n s+\omega_n^2} \f$
 */
class LowPassFilter : public DiscreteFilter<3,3>
{ 
public:

    LowPassFilter(){}
    virtual ~LowPassFilter(){}
    
    /** create butterworth filter. 
     *  zeta = sqrt(2)/2 for maximally flat response.
     * \param wn natural/cutoff frequency (rad/s)
     * \param Ts sampling period (s)
     */
    void setButterworth(double wn, double Ts)
    {
        setParameters(wn,sqrt(2)/2, Ts);
    }

    /** filter = wn^2/(s^2+2*zeta*wn*s+wn^2)
     * \param wn    natural/cutoff frequency (rad/s)
     * \param zeta  damping ratio
     * \param Ts    sampling period (s)
     */
    void setParameters(double wn, double zeta, double Ts)
    {
        wn_ = wn;
        zeta_ = zeta;
        Ts_ = Ts;

        double Ts2 = Ts*Ts;
        double wn2 = wn*wn;

        num_[0] =   Ts2*wn2;
        num_[1] = 2*Ts2*wn2;
        num_[2] =   Ts2*wn2;
        den_[0] =   Ts2*wn2 + 4*zeta*Ts*wn + 4;
        den_[1] = 2*Ts2*wn2 - 8;
        den_[2] =   Ts2*wn2 - 4*zeta*Ts*wn + 4;

        this->initialize();
    }

    /** get the cutoff frequency (rad/s) */
    double getWn(){return wn_;}

    /** get the damping ratio */
    double getZeta(){return zeta_;}

    /** get the sampling frequency (s) */
    double getTs(){return Ts_;}

private:

    double wn_   = 0;
    double zeta_ = 0;
    double Ts_   = 0;

};

}
 
#endif
