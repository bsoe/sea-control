/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_DISCRETEFILTER_H_
#define SEA_DISCRETEFILTER_H_

#include "Filter.h"

#include <cstddef>
#include <iostream>

namespace sea {

/**
 * \brief Discrete Transfer Function
 * \param N_NUM size of the numerator
 * \param N_NUM size of the denominator
 *
 * See Matlab script "C2D/C2D_symbolic.m" for converting continuous time domain transfer function
 * into discrete time transfer function, to determine the coefficients a,b. <br>
 * https://en.wikipedia.org/wiki/Digital_filter
 *
 * \f$ \frac{a_0*z[t]+a_1*z[t-1]+...+a_N*z[t-N]}{b_0*z[t]+b_1*z[t-1]+...+b_M*z[t-M]} \f$
 *
 * where: <br>
 * N_NUM = N+1 = length(a) <br>
 * N_DEN = M+1 = length(b) <br>
 * num_ = a                <br>
 * den_ = b               
 */
template <size_t N_NUM, size_t N_DEN>
class DiscreteFilter : public Filter
{ 
public:

    DiscreteFilter(){}
    virtual ~DiscreteFilter(){}

    /** \brief Update the filter
     * \param x State. */
    virtual const double& update(const double& x)
    {
        for (int i=(N_NUM-1); i>0; --i){
            x_[i] = x_[i-1];        
        }
        x_[0] = x;

        for (int i=(N_DEN-1); i>0; --i){
            y_[i] = y_[i-1];        
        }
        y_[0] = 0;

        for (int i=0; i<N_NUM; ++i){
            y_[0] += num_[i]*x_[i];
        }
        for (int i=1; i<N_DEN; ++i){
            y_[0] -= den_[i]*y_[i];
        }

        return y_[0];
    }

    /** \brief Filter output history. index of 0 is latest value */
    virtual const double& out(int index=0)
    {
        return y_[index];
    }

    /** \brief reset the time history of the IIRF. does not change the coefficients */
    virtual void resetHistory()
    {
        for (int i=0; i<N_NUM; ++i){
            x_[i] = 0;
        }
        for (int i=1; i<N_DEN; ++i){
            y_[i] = 0;
        }
    }

    /** \brief Print coefficients */
    virtual void printCoefficients()
    {
        for (int i=0;i<N_NUM;++i){
            std::cout << num_[i] << " z[-" << i << "] + ";
        }
        std::cout << '\n';
        for (int i=0;i<std::max(N_NUM,N_DEN);++i){
            std::cout << "---------------";
        }
        std::cout << '\n';
        for (int i=0;i<N_DEN;++i){
            std::cout << den_[i] << " z[-" << i << "] + ";
        }
        std::cout << std::endl;
    }

protected:

    virtual void initialize()
    {
        // normalize coefficients
        for (int i=0; i<N_NUM; ++i){
            num_[i] /= den_[0]; 
        }
        for (int i=1; i<N_DEN; ++i){
            den_[i] /= den_[0]; 
        }
        den_[0] = 1;

        // reset history
        for (int i=0; i<N_NUM; ++i){
            x_[i] = 0; 
        }
        for (int i=0; i<N_DEN; ++i){
            y_[i] = 0; 
        }
    }

    double num_[N_NUM] = {0};
    double den_[N_DEN] = {0};

    double x_[N_NUM] = {0};
    double y_[N_DEN] = {0};
};

}
 
#endif
