/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_FILTER_LEADCOMPENSATOR_H_
#define SEA_FILTER_LEADCOMPENSATOR_H_

#include "DiscreteFilter.h"

#include <stdexcept>
#include <string>

namespace sea {

/**
 * \brief Lead Compensator
 */
class LeadCompensator : public DiscreteFilter<2,2>
{ 
public:

    LeadCompensator(){}
    virtual ~LeadCompensator(){}
    
    /** \brief lead = (a1 s + a0) / (b1 s + b0)
     */
    void setCoefficients(const double& a1, const double& a0,
                         const double& b1, const double& b0,
                         const double& Ts)
    {
        C2D(a1,a0,b1,a0,Ts);
    }

    /** \brief lead = k*(s+zl)/(s+alpha*zl)
     * \param k     gain
     * \param zl    zero location
     * \param alpha pole placement factor, pl = alpha*zl
     */
    void setParameters(const double& k,const double& zl,const double& alpha,
                       const double& Ts)
    {
        C2D(k,k*zl,1,alpha*zl,Ts);
    }

    /** \brief lead = ((Wc^2 - Wo^2)*s + 2*Zo*Wo*Wc^2 - 2*Zc*Wc*Wo^2)/(Wo^2*(s + 2*Zc*Wc));
     * \param wo open loop natural frequency
     * \param zo open loop damping ratio 
     * \param wc open loop natural frequency
     * \param zc open loop damping ratio 
     */
    void setFreqDampingReshaping(const double& wo, const double& zo,
                                 const double& wc, const double& zc, 
                                 const double& Ts)
    {
        if (wc <= wo){
            throw std::runtime_error("Error. Leadcompensator::setFreqDampingReshaping(). " 
                                     "Negative derivative gain. Make sure that wc > wo.");
        }
        if ( (zo*wc - zc*wo) < 0 ){
            throw std::runtime_error("Error. Leadcompensator::setFreqDampingReshaping(). "
                                     "Negative proportional gain. Make sure wc/wo > zc/zo");
        }

        C2D(wc*wc - wo*wo, 2*zo*wo*(wc*wc) - 2*zc*wc*(wo*wo),
                             wo*wo,                   2*zc*wc*(wo*wo),
            Ts);
    }

    /** \brief lead = ((mo - mc)*s + bo - bc)/(mc*s + bc);
     * \param mo open loop mass (gear^2*m_motor)
     * \param bo open loop damping (gear^2*b_motor) 
     * \param mc closed loop mass (reshaped dynamics)
     * \param bc closed loop damping (reshaped dynamics)
     */
    void setMassDamperReshaping(const double& mo, const double& bo,
                                const double& mc, const double& bc,
                                const double& Ts)
    {
        if (mc >= mo){
            throw std::runtime_error("Error. Leadcompensator::setFreqDampingReshaping(). " 
                                     "Negative derivative gain. Make sure that mc < mo.");
        }
        if ( (bc >= bo) < 0 ){
            throw std::runtime_error("Error. Leadcompensator::setFreqDampingReshaping(). "
                                     "Negative proportional gain. Make sure bc < bo");
        }

        C2D(mo-mc,bo-bc,
            mc,   bc,
            Ts);
    }

protected:

    /** \brief initialize the z-domain coefficients from the s-domain transfer function 
     * tf(s) = a1 s + a0
     *         ---------
     *         b1 s + b0
     *
     * tf(z) = (2 a1 + Ts a0) z + Ts a0 - 2 a1
     *         -------------------------------
     *         (2 b1 + Ts b0) z + Ts b0 - 2 b1
     */
    void C2D(const double& a1, const double& a0,
                    const double& b1, const double& b0, 
                    const double& Ts)
    {
        num_[0] = 2*a1 + Ts*a0;
        num_[1] = Ts*a0 - 2*a1;
        den_[0] = 2*b1 + Ts*b0;
        den_[1] = Ts*b0 - 2*b1;

        this->initialize();
    }
};

}
 
#endif
