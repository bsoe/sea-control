/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_FILTER_DISTURBANCEOBSERVER_H_
#define SEA_FILTER_DISTURBANCEOBSERVER_H_

#include "DiscreteFilter.h"

namespace sea {

/**
 * \brief Disturbance Observer estimates the fixed plant input.
 *
 * output = Q P^-1 where Q is a 2nd order low pass filter, and P is the 2nd order mass spring damper
 * disturbance = (disturbance observer output) - (actual plant input)
 */
class DisturbanceObserver : public DiscreteFilter<3,3>
{ 
public:

    DisturbanceObserver(){}
    virtual ~DisturbanceObserver(){}
    
    /** \brief lead = (a1 s + a0) / (b1 s + b0)
     */
    void setCoefficients(const double& a2, const double& a1, const double& a0,
                         const double& b2, const double& b1, const double& b0,
                         const double& Ts)
    {
        C2D(a2,a1,a0,
            b2,b1,a0,
            Ts);
    }

    /** \brief TF = P^-1 Q
     * P = wc^2/(s^2 + 2 zc wc s + wc^2)
     * Q = wq^2/(s^2 + 2 zq wq s + wq^2)
     * \param wc closed loop natural frequency
     * \param zc closed loop damping ratio
     * \param wq low pass filter natural frequency
     * \param zq low pass filter damping ratio
     */
    void setPlant(const double& wc, const double& zc,
                  const double& wq, const double& zq,
                  const double& Ts)
    {
        // wq^2 (s^2 + 2 zc wc s + wc^2)
        // -----------------------------
        // wc^2 (s^2 + 2 zq wq s + wq^2)

        double wc2 = wc*wc;
        double wq2 = wq*wq;

        C2D(wq2, wq2*2*zc*wc, wq2*wc2,
            wc2, wc2*2*zq*wq, wc2*wq2,
            Ts);
    }

    /** \brief TF = P^-1 Q
     * P = wc^2/(s^2 + 2 zc wc s + wc^2)
     * Q = k^2/(m s^2 + b s + k)
     * \param wc closed loop natural frequency
     * \param zc closed loop damping ratio
     * \param mc closed loop mass
     * \param bc closed loop damping
     * \param k  spring
     */
    void setPlant(const double& mc, const double& bc, const double& k,
                  const double& wq, const double& zq,
                  const double& Ts)
    {
        // wq^2 (mc s^2 + bc s + k)
        // --------------------------
        // k (s^2 + 2 zq wq s + wq^2)

        double wq2 = wq*wq;

        C2D(wq2*mc,    wq2*bc, wq2*k,
                 k, k*2*zq*wq, k*wq2,
            Ts);
    }

protected:

    /** \brief initialize the z-domain coefficients from the s-domain transfer function 
     *
     * tf(s) = a2 s^2 + a1 s + a0
     *         ------------------
     *         b2 s^2 + b1 s + b0
     *
     *               2                    2         2                    2
     * tf(z) = (a0 Ts  + a1 Ts 2 + 4 a2) z  + (a0 Ts  2 - 8 a2) z + a0 Ts  - a1 Ts 2 + 4 a2
     *         ----------------------------------------------------------------------------
     *               2                    2         2                    2
     *         (b0 Ts  + b1 Ts 2 + 4 b2) z  + (b0 Ts  2 - 8 b2) z + b0 Ts  - b1 Ts 2 + 4 b2
     */
    void C2D(const double& a2, const double& a1, const double& a0,
             const double& b2, const double& b1, const double& b0,
             const double& Ts)
    {
        double Ts2 = Ts*Ts;
        num_[0] =   a0*Ts2 + 2*a1*Ts + 4*a2;
        num_[1] = 2*a0*Ts2           - 8*a2;
        num_[2] =   a0*Ts2 - 2*a1*Ts + 4*a2;
        den_[0] =   b0*Ts2 + 2*b1*Ts + 4*b2;
        den_[1] = 2*b0*Ts2           - 8*b2;
        den_[2] =   b0*Ts2 - 2*b1*Ts + 4*b2;

        this->initialize();
    }
};

}
 
#endif
