/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_DOBLEADCONTROLLER_H_
#define SEA_DOBLEADCONTROLLER_H_

#include <sea/Controller.h>
#include <sea/filter/LeadCompensator.h>
#include <sea/filter/DisturbanceObserver.h>
#include <sea/filter/LowPassFilter.h>

#include <cmath>
#include <stdexcept>
#include <time.h>

namespace sea {

/** \brief DOBLeadController base class
 *
 * Use the DOBLeadControllerBuilder to ensure the controller is valid.
 */
class DOBLeadController : public Controller
{
public:

    DOBLeadController()
    {
        enableLead(true);
        enableDOB(true);
        enable(false);

		resetTime();
    }

    DOBLeadController(const double& wo, const double& zo,
                      const double& wc, const double& zc,
                      const double& wq, const double& zq,
                      const double& Ts) 
    {
        setSampleTime       (Ts);
        setOpenLoopPlant    (wo,zo);
        setClosedLoopPlant  (wc,zc);
        setDOB              (wq,zq);

        enableLead(true);
        enableDOB(true);
        enable(false);

		resetTime();
    }

    virtual ~DOBLeadController(){}

    void setSampleTime(const double& Ts){
        Ts_ = Ts;
        updateModel();
    }

    void setOpenLoopPlant(const double& mo,
                          const double& bo,
                          const double& k){
        wo_ = sqrt(k/mo);
        zo_ = bo/(2*sqrt(mo*k));
        updateModel();
    }

    void setOpenLoopPlant(const double& wo,
                          const double& zo){
        wo_ = wo;
        zo_ = zo;
        updateModel();
    }

    void setClosedLoopPlant(const double& mc,
                            const double& bc,
                            const double& k){
        wc_ = sqrt(k/mc);
        zc_ = bc/(2*sqrt(mc*k));
        updateModel();
    }

    void setClosedLoopPlant(const double& wc,
                            const double& zc){
        wc_ = wc;
        zc_ = zc;
        updateModel();
    }

    void setDOB(const double& wq,
                const double& zq){
        wq_ = wq;
        zq_ = zq;
        updateModel();
    }

    const double& wo(){return wo_;}
    const double& zo(){return zo_;}
    const double& wc(){return wc_;}
    const double& zc(){return zc_;}
    const double& wq(){return wq_;}
    const double& zq(){return zq_;}
    const double& Ts(){return Ts_;}
	bool enabled()	  {return enabled_;}
	bool enabledLead(){return lead_enabled_;}
	bool enabledDOB() {return dob_enabled_;}

    virtual void enable(bool enable)
    {
        enabled_ = enable;
        if (enabled_ && !isValid()){
            throw std::runtime_error("DOBLeadController::enable(). Invalid parameters.");
        }
        resetHistory();
    }

    void enableLead(bool enable)
    {
        lead_enabled_ = enable;
        updateModel();
    }

    void enableDOB(bool enable)
    {
        dob_enabled_ = enable;
        updateModel();
    }

    /** \brief Update the controller
     * \param r  Reference force.
     * \param fk Spring force.
     */
    const double& update(const double& r,
                         const double& fk)
    {
		// update the time. carry over nanoseconds into microseconds.
        time_.tv_nsec += std::round(1000000000.0*Ts_);
        while (time_.tv_nsec >= 1000000000){
            time_.tv_nsec -= 1000000000;
            time_.tv_sec++;
        }

		// update the controller
        r_ = r;
        fk_ = fk;

        if (enabled_){
            if (lead_enabled_){
                if (dob_enabled_){
                    // lead + dob
                    u_Q_    = Q_.update(r_dob_);
                    u_est_  = dob_.update(fk_);
                    r_dob_  = r_ - (u_est_ - u_Q_);
                    error_  = r_dob_ - fk_;
                    u_lead_ = lead_.update(error_);
                    u_      = r_dob_ + u_lead_;
                } else {
                    // lead
                    error_  = r_ - fk_;
                    u_lead_ = lead_.update(error_);
                    u_      = r_ + u_lead_;
                }
            } else {
                if (dob_enabled_){
                    // open loop + dob
                    u_Q_    = Q_.update(r_dob_);
                    u_est_  = dob_.update(fk_);
                    r_dob_  = r_ - (u_est_ - u_Q_);
                    u_      = r_dob_;
                } else {
                    // open loop
                    u_      = r_;
                }
            }
        } else {
            u_ = 0;
        }

        if (u_ < u_min_){
            u_ = u_min_;
        } else if (u_ > u_max_) {
            u_ = u_max_;
        }

        return u_;
    }

    /** \brief Reset the time history of the controller
     * Does not reset the parameters.
     */
    virtual void resetHistory()
    {
        lead_.resetHistory();
        dob_.resetHistory();
        Q_.resetHistory();
    }

	/** \brief Returns the time */
	virtual const struct timespec& time()
	{
		return time_;
	}

	/** \brief Reset the time  to 0 */
	virtual void resetTime()
	{
		time_.tv_sec  = 0;
    	time_.tv_nsec = 0;
	}

private:

    /** \brief Returns true if all parameters are set. False otherwise. */
    bool isValid()
    {
        if (std::isnan(wo_) || std::isnan(zo_) ||
            std::isnan(wc_) || std::isnan(zc_) ||
            std::isnan(wq_) || std::isnan(zq_) ||
            std::isnan(Ts_)){
            return false;
        }
        return true;
    }

    /** \brief Updates the iirf model with the set parameters. Resets the history */
    void updateModel(){
        if (!std::isnan(wo_) && !std::isnan(zo_) &&
            !std::isnan(wc_) && !std::isnan(zc_) &&
            !std::isnan(Ts_)){
            lead_.setFreqDampingReshaping(wo_,zo_,wc_,zc_,Ts_);
        }
        if (!std::isnan(wc_) && !std::isnan(zc_) &&
            !std::isnan(wq_) && !std::isnan(zq_) &&
            !std::isnan(Ts_)){
            if (lead_enabled_){
                dob_.setPlant(wc_,zc_,wq_,zq_,Ts_);
            } else {
                dob_.setPlant(wo_,zo_,wq_,zq_,Ts_);
            }
            Q_.setParameters(wq_,zq_,Ts_);
        }
    }

    LeadCompensator lead_;
    DisturbanceObserver dob_;
    LowPassFilter Q_; 

    double lead_enabled_ = true;
    double dob_enabled_  = true;

    double r_       = 0;
    double r_dob_   = 0;
    double fk_      = 0;
    double u_       = 0;
    double u_Q_     = 0;
    double u_est_   = 0;
    double u_lead_  = 0;
    double error_   = 0;

    double wo_ = NAN;
    double zo_ = NAN;
    double wc_ = NAN;
    double zc_ = NAN;
    double wq_ = NAN;
    double zq_ = NAN;
    double Ts_ = NAN;

	struct timespec time_ = {0,0};

};

/*
class DOBLeadControllerBuilder
{
public:

    DOBLeadControllerBuilder(){}

    virtual ~DOBLeadControllerBuilder(){}

    void setSampleTime(const double& Ts){
        Ts_ = Ts;
    }

    void setOpenLoopPlant(const double& mo,
                          const double& bo,
                          const double& k){
        wo_ = sqrt(k/mo);
        zo_ = bo/(2*sqrt(mo*k));
    }

    void setOpenLoopPlant(const double& wo,
                          const double& zo){
        wo_ = wo;
        zo_ = zo;
    }

    void setClosedLoopPlant(const double& mc,
                            const double& bc,
                            const double& k){
        wc_ = sqrt(k/mc);
        zc_ = bc/(2*sqrt(mc*k));
    }

    void setClosedLoopPlant(const double& wc,
                            const double& zc){
        wc_ = wc;
        zc_ = zc;
    }

    void setDOB(const double& wq,
                const double& zq){
        wq_ = wq;
        zq_ = zq;
    }

    DOBLeadController build()
    {
        if (!isValid())
        {
            throw std::runtime_error("DOBLeadControllerBuilder. Invalid controller");
        }
        return DOBLeadController(wo_,zo_,
                                 wc_,zc_,
                                 wq_,zq_,
                                 Ts_);
    }

    bool isValid()
    {
        if (std::isnan(wo_) || std::isnan(zo_) ||
            std::isnan(wc_) || std::isnan(zc_) ||
            std::isnan(wq_) || std::isnan(zq_) ||
            std::isnan(Ts_)){
            return false;
        }
        return true;
    }

    const double& wo(){return wo_;}
    const double& zo(){return zo_;}
    const double& wc(){return wc_;}
    const double& zc(){return zc_;}
    const double& wq(){return wq_;}
    const double& zq(){return zq_;}
    const double& Ts(){return Ts_;}

protected:

    double wo_ = NAN;
    double zo_ = NAN;
    double wc_ = NAN;
    double zc_ = NAN;
    double wq_ = NAN;
    double zq_ = NAN;
    double Ts_ = NAN;

};
*/

}

#endif
