/*
Copyright 2017 Brian Soe

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SEA_FRICTION_COULOMBFRICTIONCOMPENSATOR_H_
#define SEA_FRICTION_COULOMBFRICTIONCOMPENSATOR_H_
 
#include "FrictionCompensator.h"

#include <cmath>
#include <stdexcept>

namespace sea {

/**
 * \brief Class that calculates Coulomb friction from velocity
 * with tanh smoothing function around 0 velocity
 *
 * Friction is modeled similar to coulomb*tanh(2/buffer*velocity+offset)+coulomb/2
 * such that:
 * dfriction/dvelocity @ velocity=0 is buffer
 * friction @ buffer = 96% of coulomb
 *
 * Actually, it is more complicated because the coulomb and buffer upper/lower limits can be different.
 * Look at the source code if interested.
 *
 * https://www.wolframalpha.com/input/?i=tanh(x)
 */
class CoulombFrictionCompensator : public FrictionCompensator
{ 
public:

    CoulombFrictionCompensator(){}

    virtual ~CoulombFrictionCompensator(){}
    
    virtual double friction(const double& velocity)
    {
		if (enabled_)
		{
		    if (coulomb_positive_==0 && coulomb_negative_==0){
		        friction_ = 0;
		    }

		    double a = (coulomb_positive_-coulomb_negative_)/2.0;
		    double b = (coulomb_positive_+coulomb_negative_)/2.0;
		    double c = 2.0/buffer_;
		    double d = atanh(b/a);
		    friction_ = a*tanh(c*velocity-d)+b;

		} else {
			friction_ = 0;
		}

        return friction_;
    }

    void setCoulombPositive(const double& force)
    {
        if (force >= 0){
            coulomb_positive_ = force;
        } else {
            throw std::runtime_error("setCoulombPositive accepts values greater than or equal to zero");
        }
    }

    void setCoulombNegative(const double& force)
    {
        if (force <= 0){
            coulomb_negative_ = force;
        } else {
            throw std::runtime_error("setCoulombNegative accepts values less than or equal to zero");
        }
    }

    void setBuffer(const double& buffer)
    {
        if (buffer >= 0){
            buffer_ = buffer;
        } else {
            throw std::runtime_error("setBufferPositive accepts values greater than or equal to zero");
        }
    }

private:

    double friction_ = 0;
    double coulomb_positive_ = 1;
    double coulomb_negative_ = -1;
    double buffer_ = 1;

};

}
 
#endif
