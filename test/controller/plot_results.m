clc; clear; close all;

%% import data
data_file = 'build/test1.txt'; % fixed load - open loop
%data_file = 'build/test2.txt'; % fixed load - open loop + DOB
%data_file = 'build/test3.txt'; % fixed load - lead compensator
%data_file = 'build/test4.txt'; % fixed load - lead + DOB
%data_file = 'build/test5.txt'; % free  load - open loop
%data_file = 'build/test6.txt'; % free  load - open loop + DOB
%data_file = 'build/test7.txt'; % free  load - lead comensator
%data_file = 'build/test8.txt'; % free  load - lead + DOB

wlim = {0.01,100};

data = importdata(data_file); 
time = data(:,1);
ref  = data(:,2);
fk   = data(:,3);
u    = data(:,4);

N    = length(time);
Ts   = time(2)-time(1);

%% plot time history
figure
plot(time,ref,...
     time,fk,...
     time,u)
xlabel('time (s)')
legend('reference','out','command')

%% frequency response
H = etfe( iddata(fk,ref,Ts), [], N );

figure;
bode(H,wlim); hold on;

%% ideal frequency response
k  = 1;
mo = 1;
bo = 0.2*2*sqrt(mo*k);
ml = mo;
bl = 0.1*bo;

s = tf('s');
P_fixed = k/(mo*s^2+bo*s+k);
alpha   = (ml*s^2+bl*s)/(ml*s^2+bl*s+k);
P_free  = k*alpha/(mo*s^2+bo*s+k*alpha);

bode(P_fixed,':r')
bode(P_free,':y')

legend('etfe','ideal fixed','ideal free')







