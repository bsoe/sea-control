/****************************************************************************/

#include <sea/sim/SEASimulation.h>
#include <sea/sim/EulerIntegrator.h>
#include <sea/sim/RungeKuttaIntegrator.h>
#include <sea/DOBLeadController.h>
#include <sea/Chirp.h>

#include <fstream>
#include <iostream>


int main(int argc, char **argv)
{
    double Ts = 0.001;

    // Design Chirp
    sea::Chirp chirp;
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(1.0);
    chirp.setFrequencyRange(0.01,50.0);
    chirp.setDuration(1000.0);
 
    // Simulation
    sea::SEASimulation<sea::RungeKuttaIntegrator> sim;
    sim.setTimeStep(Ts);

    const std::vector<double> x0 = {0,0,0,0};
    sim.setInitialCondition(x0);

    sim.k_       = 1;
    sim.m_motor_ = 1;
    sim.b_motor_ = 0.2*2*sqrt(sim.m_motor_*sim.k_);
    sim.m_link_  = sim.m_motor_;
    sim.b_link_  = 0.1*sim.b_motor_;

    // Controller
    sea::DOBLeadController controller;
    controller.setSampleTime(Ts);
    controller.setOpenLoopPlant(sim.m_motor_,sim.b_motor_,sim.k_);
    controller.setClosedLoopPlant(10*controller.wo(),sqrt(2.0)/2.0);
    controller.setDOB(controller.wc(),sqrt(2.0)/2.0);
    controller.enable(true);

    // ==========================================
    // Fixed load & Open Loop

    std::cout << "Simlulating Fixed Load SEA with Open Loop" << std::endl;

    sim.fixed_output_ = true;

    controller.enableLead(false);
    controller.enableDOB(false);

    std::ofstream myfile1;
    myfile1.open ("test1.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile1 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile1.close();

    // ==========================================
    // Fixed load & Open Loop + DOB

    std::cout << "Simlulating Fixed Load SEA with Open Loop + DOB" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = true;

    controller.enableLead(false);
    controller.enableDOB(true);

    std::ofstream myfile2;
    myfile2.open ("test2.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile2 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile2.close();

    // ==========================================
    // Fixed Load & Lead Compensator

    std::cout << "Simlulating Fixed Load SEA with Lead Compensator" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = true;

    controller.enableLead(true);
    controller.enableDOB(false);

    std::ofstream myfile3;
    myfile3.open ("test3.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile3 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile3.close();

    // ==========================================
    // Fixed Load & Lead + DOB

    std::cout << "Simlulating Fixed Load SEA with Lead + DOB" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = true;

    controller.enableLead(true);
    controller.enableDOB(true);

    std::ofstream myfile4;
    myfile4.open ("test4.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile4 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile4.close();

    // ==========================================
    // Free Load & Open Loop

    std::cout << "Simlulating Free  Load SEA with Open Loop" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = false;

    controller.enableLead(false);
    controller.enableDOB(false);

    std::ofstream myfile5;
    myfile5.open ("test5.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile5 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile5.close();

    // ==========================================
    // Free Load & Open Loop + DOB

    std::cout << "Simlulating Free  Load SEA with Open Loop + DOB" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = false;

    controller.enableLead(false);
    controller.enableDOB(true);

    std::ofstream myfile6;
    myfile6.open ("test6.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile6 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile6.close();

    // ==========================================
    // Free Load & Lead Compensator

    std::cout << "Simlulating Free  Load SEA with Lead Compensator" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = false;

    controller.enableLead(true);
    controller.enableDOB(false);

    std::ofstream myfile7;
    myfile7.open ("test7.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile7 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile7.close();

    // ==========================================
    // Fixed Load & Lead + DOB

    std::cout << "Simlulating Free  Load SEA with Lead + DOB" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = false;

    controller.enableLead(true);
    controller.enableDOB(true);

    std::ofstream myfile8;
    myfile8.open ("test8.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = controller.update(r,sim.force());
        sim.step();

        myfile8 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\n';
    }

    myfile8.close();


    return 0;
}

/****************************************************************************/
