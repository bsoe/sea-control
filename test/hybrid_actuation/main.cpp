/****************************************************************************/

#include <sea/sim/SEASimulation.h>
#include <sea/sim/EulerIntegrator.h>
#include <sea/sim/RungeKuttaIntegrator.h>
#include <sea/DOBLeadController.h>
#include <sea/filter/HybridCompensator.h>
#include <sea/Chirp.h>

#include <fstream>
#include <iostream>


int main(int argc, char **argv)
{
    double Ts = 0.001;

    // Design Chirp
    sea::Chirp chirp;
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(1.0);
    chirp.setFrequencyRange(0.0005,50.0);
    chirp.setDuration(10000.0);
 
    // Simulation
    sea::SEASimulation<sea::RungeKuttaIntegrator> sim;
    sim.setTimeStep(Ts);

    const std::vector<double> x0 = {0,0,0,0};
    sim.setInitialCondition(x0);

    sim.k_       = 1;
    sim.m_motor_ = sim.k_/pow(0.01,2); //1
    sim.b_motor_ = 0.2*2*sqrt(sim.m_motor_*sim.k_);
    sim.m_link_  = 1.0;
    sim.b_link_  = 0.0;;

    // SEA Controller
    sea::DOBLeadController sea_controller;
    sea_controller.setSampleTime(Ts);
    sea_controller.setOpenLoopPlant(sim.m_motor_,sim.b_motor_,sim.k_);
    sea_controller.setClosedLoopPlant(10*sea_controller.wo(),sqrt(2.0)/2.0);
    sea_controller.setDOB(sea_controller.wc(),sqrt(2.0)/2.0);
    sea_controller.enable(true);

    // Direct drive controller
    sea::HybridCompensator dd_controller;
    dd_controller.setPlant(sea_controller.wc(),sea_controller.zc(),Ts);

    // printout
    double wo = sqrt(sim.k_/sim.m_motor_);
    double zo = sim.b_motor_/(2*sqrt(sim.m_motor_*sim.k_));
    double wc = sea_controller.wc();
    double zc = sea_controller.zc();
    double wl = sqrt(sim.k_/sim.m_link_);
    double zl = sim.b_link_/(2*sqrt(sim.m_link_*sim.k_));

    std::cout << "(wo,zo) = " << "(" << wo << "," << zo << ")" << std::endl;
    std::cout << "(wc,zc) = " << "(" << wc << "," << zc << ")" << std::endl;
    std::cout << "(wl,zl) = " << "(" << wl << "," << zl << ")" << std::endl;

    std::string file_name = std::string("hybrid_actuation_force_response_wcwl_of_")+std::to_string(wc/wl);
    file_name.erase( file_name.find_last_not_of('0') + 1, std::string::npos );

 
    // ==========================================
    // Simulation of Free Load & Lead + DOB
    
    std::cout << "Simlulating Free  Load SEA with Lead + DOB" << std::endl;

    sim.setInitialCondition(x0);
    sim.setTime(0);
    sim.fixed_output_ = false;

    sea_controller.resetHistory();
    sea_controller.enableLead(true);
    sea_controller.enableDOB(true);

    dd_controller.resetHistory();

    std::ofstream myfile8;
    //myfile8.open (file_name+"_lead_DOB.txt");
    myfile8.open (file_name+".txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = sea_controller.update(r,sim.force());
        sim.d_ = dd_controller.update(r);
        sim.step();

        myfile8 << sim.time()           << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << sim.u_               << '\t'
                << sim.disturbance()    << '\n';
    }

    myfile8.close();


    return 0;
}

/****************************************************************************/
