function getBodeMagAxes()

children = get(gcf, 'Children'); % use this handle to obtain list of figure's children
magChild = children(3);          % Pick a handle to axes of magnitude in bode diagram.
%magChild = childern(2)         % This way you can add data to Phase Plot.
axes(magChild)                   % Make those axes current