clc; clear; close all;

%% free response

wcwl = {'0.1' '0.316' '1.0' '3.16' '10.0'};

figure
for i = [1:length(wcwl)]

    %% import data
    data_file = ['build/hybrid_actuation_force_response_wcwl_of_' wcwl{i} '.txt']

    wlim = {0.01,10};

    data = importdata(data_file); 
    time = data(:,1);
    ref  = data(:,2);
    fk   = data(:,3);
    u    = data(:,4);
    d    = data(:,5);
    
    N    = length(time);
    Ts   = time(2)-time(1);

    %% frequency response
    H = etfe( iddata(fk+d,ref,Ts), [], N );
    bode(H,wlim); hold on;
end

title('Hybrid Actuator Force Response')

legend_entries = wcwl;
for i=1:length(wcwl)
    legend_entries{i} = ['\omega_c/\omega_l = ' legend_entries{i}];
end
getBodeMagAxes();
legend(legend_entries,'location','NorthWest');
legend(legend_entries,'location','NorthWest');

addpath('../../../export_fig')
set(gcf, 'Color', 'w');
export_fig('hybrid_actuation_response.png', '-m3');   






