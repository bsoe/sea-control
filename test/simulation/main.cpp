/****************************************************************************/

#include <sea/sim/SEASimulation.h>
#include <sea/sim/EulerIntegrator.h>
#include <sea/sim/RungeKuttaIntegrator.h>
#include <sea/Chirp.h>

#include <fstream>
#include <iostream>


int main(int argc, char **argv)
{
    double Ts = 0.001;

    // Design Chirp
    sea::Chirp chirp;
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(1.0);
    chirp.setFrequencyRange(0.01,10.0);
    chirp.setDuration(1000.0);
 
    // Simulation
    sea::SEASimulation<sea::EulerIntegrator> sim;
    sim.setTimeStep(Ts);

    const std::vector<double> x0 = {0,0,0,0};
    sim.setInitialCondition(x0);

    sim.k_       = 1;
    sim.m_motor_ = 1;
    sim.b_motor_ = 0.2*2*sqrt(sim.m_motor_*sim.k_);
    sim.m_link_  = sim.m_motor_;
    sim.b_link_  = 0.1*sim.b_motor_;

    // ==========================================
    // Fixed load

    sim.fixed_output_ = true;

    std::ofstream myfile1;
    myfile1.open ("test1.txt");

    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = r;
        sim.step();

        myfile1 << sim.time()    << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << chirp.frequency()   << '\n';
    }

    myfile1.close();

    // ==========================================
    // Free Load

    sim.setInitialCondition(x0);
    sim.setTime(0);

    sim.fixed_output_ = false;

    std::ofstream myfile2;
    myfile2.open ("test2.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim.u_ = r;
        sim.step();

        myfile2 << sim.time()    << '\t'
                << r                    << '\t'
                << sim.force()          << '\t'
                << chirp.frequency()   << '\n';
    }

    myfile2.close();

    // ==========================================
    // Free Load with RungeKutta

    std::cout << "free load runge kutta" << std::endl;

    // Simulation
    sea::SEASimulation<sea::RungeKuttaIntegrator> sim3;

    sim3.setTimeStep(Ts);
    sim3.setInitialCondition(x0);

    sim3.k_       = sim.k_;
    sim3.m_motor_ = sim.m_motor_;
    sim3.b_motor_ = sim.b_motor_;
    sim3.m_link_  = sim.m_link_;
    sim3.b_link_  = sim.b_link_;

    std::ofstream myfile3;
    myfile3.open ("test3.txt");

    chirp.resetTime(0.0);
    while(chirp.time() < chirp.duration())
    {
        double r = chirp.step();
        sim3.u_ = r;
        sim3.step();

        myfile3 << sim3.time()              << '\t'
                << r                        << '\t'
                << sim3.force()             << '\t'
                << chirp.frequency()       << '\t'
                << sim3.numberOfSubsteps()  << '\n';
    }

    myfile3.close();


    return 0;
}

/****************************************************************************/
