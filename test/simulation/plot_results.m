clc; clear; close all;

%% import data
data_file = 'build/test1.txt';
%data_file = 'build/test2.txt';
%data_file = 'build/test3.txt';

wlim = {0.01,100};

data = importdata(data_file); 
time = data(:,1);
in   = data(:,2);
out  = data(:,3);
freq = data(:,4);
N = length(time);
Ts = time(2)-time(1);

%% plot time history
figure
plot(time,in,...
     time,out)
xlabel('time (s)')
legend('in','out')

figure
semilogy(time,freq)
xlabel('time (s)')
ylabel('frequency (rad/s)')

%% frequency response
H = etfe( iddata(out,in,Ts), [], N );

figure;
bode(H,wlim); hold on;

%% ideal frequency response
k  = 1;
mo = 1;
bo = 0.2*2*sqrt(mo*k);
ml = mo;
bl = 0.1*bo;

s = tf('s');
P_fixed = k/(mo*s^2+bo*s+k);
alpha   = (ml*s^2+bl*s)/(ml*s^2+bl*s+k);
P_free  = k*alpha/(mo*s^2+bo*s+k*alpha);

bode(P_fixed,':r')
bode(P_free,':y')

legend('etfe','ideal fixed','ideal free')

%% number of substeps for runge kutta
if strcmp(data_file,'build/test3.txt')
    substeps = data(:,5);
    figure
    plot(time,substeps)
    xlabel('time (s)')
    ylabel('substeps')
    title('runge kutta')
end







