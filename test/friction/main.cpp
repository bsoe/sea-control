/****************************************************************************/

#include <sea/friction/CoulombFrictionCompensator.h>

#include <fstream>
#include <iostream>

/****************************************************************************/

int main(int argc, char **argv)
{
    sea::CoulombFrictionCompensator friction_compensator;
    friction_compensator.setCoulombPositive( 75.0);
    friction_compensator.setCoulombNegative(-75.0);
    friction_compensator.setBuffer(5.0);

    for (double vel = -10.0; vel <= 10.0; vel+=0.1){
        std::cout << vel << "\t" << friction_compensator.friction(vel) << std::endl;
    }

    return 0;
}

/****************************************************************************/
