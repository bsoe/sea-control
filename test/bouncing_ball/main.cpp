/****************************************************************************/

#include <sea/sim/BouncingBallSimulation.h>
#include <sea/sim/EulerIntegrator.h>
#include <sea/sim/RungeKuttaIntegrator.h>

#include <fstream>
#include <iostream>


int main(int argc, char **argv)
{
    double Ts = 0.001;
 
    // Simulation
    sea::BouncingBallSimulation<sea::RungeKuttaIntegrator> sim;
    sim.setTimeStep(Ts);

    const std::vector<double> x0 = {-5,0};
    sim.setInitialCondition(x0);

    sim.m_       = 1;   // mass
    sim.b_       = 0;   // damping
    sim.k_       = 1;   // spring
    sim.g_       = 10;  // gravity acceleration
    sim.d_       = 0;   // disturbance force

    // ==========================================
    // Fixed load

    std::ofstream myfile;
    myfile.open ("test1.txt");

    while(sim.time() < 10)
    {
        sim.step();
        std::vector<double> x = sim.x();

        myfile << sim.time()    << '\t'
                << sim.x(0)     << '\t'
                << sim.x(1)     << '\t'
                << sim.springForce()  << '\n';
    }

    myfile.close();

    return 0;
}

/****************************************************************************/
