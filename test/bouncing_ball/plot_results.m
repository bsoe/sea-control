clc; clear; close all;

%% import data
data_file = 'build/test1.txt';

data    = importdata(data_file); 
time    = data(:,1);
x       = data(:,2);
dx      = data(:,3);
fk      = data(:,4);
N       = length(time);

%% plot time history
figure
subplot(2,1,1)
plot(time,x,...
     time,dx)
ylabel('trajectory')
legend('x','dx')

subplot(2,1,2)
plot(time,fk)
ylabel('spring force')
xlabel('time')





