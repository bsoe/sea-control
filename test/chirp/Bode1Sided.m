function [f, X, mag, pha] = Bode1Sided(x,Fs)
% BODE1SIDED Compute the 1-sided fft.
%   [f, X, mag, pha] = Bode1Sided(x,Fs) 
%   computes the fft from a time vector x, and sampling freqeuncy Fs (Hz)
%   returns the frequencies (Hz), complex coefficients, magnitude, and phase
%
%   See also fft.
%
% https://www.mathworks.com/help/matlab/ref/fft.html

N = length(x);
f = Fs*(0:floor(N/2))/N;

X2 = fft(x)/N;
X = X2(1:floor(N/2)+1);
X(2:end-1) = 2*X(2:end-1);

mag = abs(X);
pha = unwrap(angle(X));

end
