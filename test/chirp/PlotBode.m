function PlotBode(f,X,semilog)
% PLOTBODE Plots the 1-sided fft
%   PlotBode(f,X,semilog) 
%   plots the bode from the frequencies f (Hz) and complex coefficients X
%
%   See also fft.
%
% https://www.mathworks.com/help/matlab/ref/fft.html


subplot(2,1,1)
if (nargin>2 && semilog)
    semilogx(f,abs(X)); hold on;
    ylabel('Magnitude')
else
    semilogx(f,20*log10(abs(X)));
    ylabel('Magnitude (dB)')
end
subplot(2,1,2)
semilogx(f,180/pi*unwrap(angle(X))); hold on;
xlabel 'Frequency (Hz)'
ylabel 'Phase (deg)'
grid
