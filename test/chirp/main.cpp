/****************************************************************************/

#include <sea/Chirp.h>

#include <fstream>
#include <iostream>
#include <cmath>

/****************************************************************************/

int main(int argc, char **argv)
{
    // test 1
    sea::Chirp chirp;
    chirp.setSamplingTime(0.001);
    chirp.setAmplitude(1.0);
    chirp.setFrequencyRange(0.1,10.0);
    chirp.setDuration(100.0);
    
    std::ofstream myfile;
    myfile.open ("chirp1.txt");
    while(chirp.time() < chirp.duration()){
        chirp.step();
        myfile << chirp.time() << '\t'
               << chirp.value() << '\n';
    }
    myfile.close();

    // test 2
    chirp.setDuration(100.0,5,5);
    chirp.resetTime(0.0);
    
    myfile.open ("chirp2.txt");
    while(chirp.time() < chirp.duration()){
        chirp.step();
        myfile << chirp.time() << '\t'
               << chirp.value() << '\n';
    }
    myfile.close();

    return 0;
}

/****************************************************************************/
