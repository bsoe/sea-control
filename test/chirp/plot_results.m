clc; clear; close all;

%% test 1
data_file = 'build/chirp1.txt';
data = importdata(data_file);

figure
plot(data(:,1),data(:,2))
title('test 1')

figure
Fs = 1/(data(2,1)-data(1,1))
[f,X, ~, ~] = Bode1Sided(data(:,2),Fs);
PlotBode(f,X);

%% test 2
data_file = 'build/chirp2.txt';
data = importdata(data_file);

figure
plot(data(:,1),data(:,2))
title('test 1')

figure
Fs = 1/(data(2,1)-data(1,1))
[f,X, ~, ~] = Bode1Sided(data(:,2),Fs);
PlotBode(f,X);