clc; clear; close all;

%% import data
data_file = 'build/test1.txt';
%data_file = 'build/test2.txt';

wlim = {0.01,100};

data = importdata(data_file); 
time = data(:,1);
in   = data(:,2);
out0 = data(:,3);
out1 = data(:,4);
out2 = data(:,5);
N = length(time);
Ts = time(2)-time(1);

%% plot time history
figure
plot(time,in,...
     time,out0,...
     time,out1,...
     time,out2)
xlabel('time (s)')
legend('in','out0','out1','out2')

%% ideal frequency response
wn = 2*pi*6.5;
zeta = 0.2;

s = tf('s');
G_ideal = wn^2/(s^2+2*zeta*wn*s+wn^2);

%% frequency response
H0 = etfe( iddata(out0,in,Ts), [], N );
H1 = etfe( iddata(out1,in,Ts), [], N );
H2 = etfe( iddata(out2,in,Ts), [], N );

figure;
bode(H0,wlim); hold on;
bode(H1,wlim);
bode(H2,wlim);
legend('H0','H1','H2')


