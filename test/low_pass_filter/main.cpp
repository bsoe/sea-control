/****************************************************************************/

#include <sea/filter/LowPassFilter.h>
#include <sea/Chirp.h>

#include <fstream>
#include <iostream>

/****************************************************************************/

int main(int argc, char **argv)
{
    double Ts = 0.001;

    // Design Chirp
    sea::Chirp chirp;
    chirp.setSamplingTime(Ts);
    chirp.setAmplitude(1.0);
    chirp.setFrequencyRange(0.01,100.0);
    chirp.setDuration(1000.0);
 
    // TEST 1, try different natural frequencies

    sea::LowPassFilter filter0;
    sea::LowPassFilter filter1;
    sea::LowPassFilter filter2;

    filter0.setParameters( 0.1, 1, Ts);
    filter1.setParameters( 1.0, 1, Ts);
    filter2.setParameters(10.0, 1, Ts);

    std::ofstream myfile1;
    myfile1.open ("test1.txt");

    while(chirp.time() < chirp.duration())
    {
        double in   = chirp.step();
        double out0 = filter0.update(in);
        double out1 = filter1.update(in);
        double out2 = filter2.update(in);
        myfile1 << chirp.time() << '\t' 
                << in   << '\t'
                << out0 << '\t'
                << out1 << '\t'
                << out2 << '\n';
    }

    myfile1.close();

    // TEST 2, try different damping ratios

    chirp.resetTime(0.0);

    sea::LowPassFilter filter3;
    sea::LowPassFilter filter4;
    sea::LowPassFilter filter5;

    filter3.setParameters( 1, 0.1, Ts);
    filter4.setButterworth(1, Ts);
    filter5.setParameters( 1, 1.0, Ts);

    std::ofstream myfile2;
    myfile2.open ("test2.txt");

    while(chirp.time() < chirp.duration())
    {
        double in   = chirp.step();
        double out0 = filter3.update(in);
        double out1 = filter4.update(in);
        double out2 = filter5.update(in);
        myfile2 << chirp.time() << '\t' 
                << in   << '\t'
                << out0 << '\t'
                << out1 << '\t'
                << out2 << '\n';
    }

    myfile2.close();

    return 0;
}

/****************************************************************************/
