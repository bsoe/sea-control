clc; close all; clear;

%% parameters
Wn = 2*pi*50;
zeta = sqrt(2)/2;

fs = 1000;

%% transfer functions
s = tf('s');
lpfilter = Wn^2/(s^2+2*zeta*Wn*s+Wn^2);

obj = DiscreteFilter(lpfilter, fs, Wn);
obj.tf_s
obj.tf_z
obj.plot()

%% time domain

freq_real = logspace(-1,2.5,30);

num_freq = length(freq_real);
amp_sim = zeros(1,num_freq);
freq_sim = zeros(1,num_freq);
phase_sim = zeros(1,num_freq);

num_sinusoids = 4;

for i=1:num_freq
    
    f = freq_real(i);

    t= [0:1/fs:num_sinusoids/f];
    x = sin(2*pi*f*t);

    n = length(t);
    y = zeros(1,n);

    % run filter
    for j=1:n
        y(j) = obj.update(x(j));
    end
    
    [amplitude, frequency, phase, bias] = SineFit(t,y);
    
    z = amplitude*sin(2*pi*frequency*t+phase)+bias;

    figure(2);  hold off;
    plot(t,x,t,y); hold on;
    plot(t,z,':'); 
    legend('reference','filtered','fitted');
    ylim([-1 1]);
    title(sprintf('frequency %0.1f',f));
    
    amp_sim(i) = amplitude;
    freq_sim(i) = frequency;
    phase_sim(i) = phase;
    
    pause(1);
    
end

%% frequency domain

figure

subplot(2,1,1)
semilogx(freq_sim,20*log10(amp_sim))
ylabel('magnitude (dB)')

subplot(2,1,2)
semilogx(freq_sim,180/pi*phase_sim)
ylabel('phase (deg)')

xlabel('frequency (Hz)')



