clc; clear;

%% s-domain tf - user specified
syms s

Ts_eval = 0.001;

%syms wn zeta;   
%tf_s = wn^2/(s^2+2*zeta*wn*s+wn^2);   
%parameters  = {wn zeta};
%values      = {2*pi*20 sqrt(2)/2};


%syms mo bo mc bc;   
%tf_s = ((mo-mc)*s+(bo-bc))/(mc*s+bc);   
%parameters  = {mo bo mc bc};
%values      = {1 0.5*2*sqrt(1*1) 0.1 0.707*2*sqrt(0.1*1)};

%syms a0 a1 b0 b1;   
%tf_s = (a1*s+a0)/(b1*s+b0);   
%parameters  = {a1 a0 b1 b0};
%values      = {(1-0.1) (0.5-0.707*2*sqrt(0.1*1)) 0.1 0.707*2*sqrt(0.1*1)};

syms a0 a1 a2 b0 b1 b2;   
tf_s = (a2*s^2+a1*s+a0)/(b2*s^2+b1*s+b0);   
parameters  = {a2 a1 a0 b2 b1 b0};
values      = {1 1.5 2 2 1.5 1};


%% bilinear tf
syms z Ts;

tf_z = subs(tf_s, s, 2/Ts*(z-1)/(z+1));
tf_z = collect(simplify(tf_z),z);
pretty(tf_z)

%% numeric values
tf_z = subs(tf_z,[Ts parameters],[Ts_eval values]);
tf_z = sym2tf(tf_z)

%% verification
s = tf('s');
tf_s = subs(tf_s,parameters,values);
tf_s = sym2tf(tf_s);
tf_z = c2d(tf_s,Ts_eval,'tustin')

tf_s